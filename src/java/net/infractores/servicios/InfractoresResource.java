package net.infractores.servicios;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Properties;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.infractores.entity.AdjuntoEntity;
import net.infractores.entity.CustodioEntity;
import net.infractores.entity.CustodioReporteEntity;
import net.infractores.entity.DominioEntity;
import net.infractores.entity.InfractorEntity;
import net.infractores.entity.RenovacionEntity;
import net.infractores.entity.SancionEntity;
import net.infractores.entity.TitularEntity;
import net.infractores.entity.TituloEntity;
import net.infractores.entity.UbigeoEntity;
import net.infractores.entity.UsuarioEntity;
import net.infractores.mybatis.dao.CustodioDAOSQL;
import net.infractores.mybatis.dao.InfractorDAOSQL;
import net.infractores.mybatis.dao.UsuarioDAOSQL;
import net.infractores.negocio.CustodioNegocio;
import net.infractores.negocio.InfractorNegocio;
import net.infractores.negocio.UsuarioNegocio;
import net.infractores.request.LoginRequestEntity;
import net.infractores.response.AdjuntoListadoResponseEntity;
import net.infractores.response.CustodioListadoResponseEntity;
import net.infractores.response.DominioResponseEntity;
import net.infractores.response.InfractorListadoResponseEntity;
import net.infractores.response.RenovacionListadoResponseEntity;
import net.infractores.response.ResponseEntity;
import net.infractores.response.SancionResponseEntity;
import net.infractores.response.TitularListadoResponseEntity;
import net.infractores.response.TituloListadoResponseEntity;
import net.infractores.response.UbigeoResponseEntity;
import net.infractores.response.UsuarioResponseEntity;
import net.infractores.utils.Constante;
import net.infractores.utils.FechaUtils;
import net.infractores.utils.GeneralUtil;
import net.infractores.utils.Log;

@SuppressWarnings({"BoxedValueEquality", "NumberEquality","CallToPrintStackTrace", "UnusedAssignment", "Convert2Diamond", "OverridableMethodCallInConstructor", "SleepWhileInLoop", "null"})
@Path("infractores")
public class InfractoresResource {
    @Context
    private UriInfo context;
    private String nombreLog;
    private String rutaJSONData;
    private String rutaLOG;
    
    public static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789{}!#$%&.,/()='¿?|-[]+";
    public static final int SECURE_TOKEN_LENGTH = 256;
    private static final SecureRandom random = new SecureRandom();
    private static final char[] symbols = CHARACTERS.toCharArray();
    private static final char[] buf = new char[SECURE_TOKEN_LENGTH];
    
    public InfractoresResource() {
        nombreLog = Constante.LOG_REST_SERVICE_GENERAL;
    }
    
    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UsuarioResponseEntity Login(LoginRequestEntity datosUsuario) throws Exception {
        UsuarioResponseEntity response = new UsuarioResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            UsuarioDAOSQL daoUsuario = new UsuarioDAOSQL();
            UsuarioNegocio negocioUsuario = new UsuarioNegocio(daoUsuario);
            
            UsuarioEntity usuario = negocioUsuario.validarUsuario(datosUsuario.getUsuario(), datosUsuario.getClave());
            if (usuario.getUsuarioId() < 0) {
                if (null != usuario.getUsuarioId()) switch (usuario.getUsuarioId()) {
                    case -1:
                        response.setResult_code(Constante.ERROR_USUARIO_DATOS_INCORRECTOS);
                        response.setError_description(Constante.DESCRIPCION_ERROR_USUARIO_DATOS_INCORRECTOS);
                        break;
                    case -2:
                        response.setResult_code(Constante.ERROR_USUARIO_SIN_ROLES);
                        response.setError_description(Constante.DESCRIPCION_ERROR_USUARIO_SIN_ROLES);
                        break;
                    default:
                        response.setResult_code(Constante.ERROR_SERVIDOR);
                        response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + usuario.getUsuarioId());
                        break;
                }
            } else {
                usuario.setRoles(negocioUsuario.rolesUsuario(usuario.getUsuarioId()));
                
                response.setUsuario(usuario);
                response.setAccessToken(nextToken());
            }
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crudPrincipal")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity CrudPrincipal(InfractorEntity infractor) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            Integer infractorId = negocioInfractor.crudInfractorPrincipal(infractor);
            response.setCode(infractorId);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crudSecundario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity CrudSecundario(InfractorEntity infractor) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            negocioInfractor.crudInfractorSecundario(infractor);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crear")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity Crear(InfractorEntity infractor) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            negocioInfractor.mantenimientoInfractor("I", infractor);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("modificar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity Modificar(InfractorEntity infractor) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            negocioInfractor.mantenimientoInfractor("U", infractor);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("activar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity Activar(InfractorEntity infractor) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            negocioInfractor.mantenimientoInfractor("E", infractor);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("desactivar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity Desactivar(InfractorEntity infractor) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            negocioInfractor.mantenimientoInfractor("D", infractor);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("listado")
    @Produces(MediaType.APPLICATION_JSON)
    public InfractorListadoResponseEntity Listado(@QueryParam("buscar") String buscar, @QueryParam("desde") String desde, @QueryParam("hasta") String hasta, @QueryParam("pagina") Integer pagina, @QueryParam("regxpag") Integer regxpag) throws Exception {
        InfractorListadoResponseEntity response = new InfractorListadoResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            List<InfractorEntity> infractores = negocioInfractor.listadoInfractores(buscar, desde, hasta, pagina, regxpag);
            
            response.setContent(infractores);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("ubigeos")
    @Produces(MediaType.APPLICATION_JSON)
    public UbigeoResponseEntity Ubigeos(@QueryParam("tipo") String tipo, @QueryParam("departamento") String departamento, @QueryParam("provincia") String provincia) throws Exception {
        UbigeoResponseEntity response = new UbigeoResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            List<UbigeoEntity> ubigeos = negocioInfractor.listadoUbigeos(tipo, departamento, provincia);
            
            response.setUbigeos(ubigeos);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("adjuntos")
    @Produces(MediaType.APPLICATION_JSON)
    public AdjuntoListadoResponseEntity Adjuntos(@QueryParam("infractorId") Integer infractorId, @QueryParam("pagina") Integer pagina, @QueryParam("regxpag") Integer regxpag) throws Exception {
        AdjuntoListadoResponseEntity response = new AdjuntoListadoResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            List<AdjuntoEntity> adjuntos = negocioInfractor.listadoInfractorAdjuntos(infractorId, pagina, regxpag);
            
            response.setContent(adjuntos);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("sanciones")
    @Produces(MediaType.APPLICATION_JSON)
    public SancionResponseEntity Sanciones(@QueryParam("infractorId") Integer infractorId) throws Exception {
        SancionResponseEntity response = new SancionResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            List<SancionEntity> sanciones = negocioInfractor.listadoInfractorSanciones(infractorId);
            
            response.setSanciones(sanciones);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crudSancion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity CrudSancion(SancionEntity sancion) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            negocioInfractor.mantenimientoInfractorSancion(sancion);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("infraccion")
    @Produces(MediaType.APPLICATION_JSON)
    public DominioResponseEntity Infraccion() throws Exception {
        DominioResponseEntity response = new DominioResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            List<DominioEntity> infracciones = negocioInfractor.listadoInfracciones();
            
            response.setDetalle(infracciones);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("caducidad")
    @Produces(MediaType.APPLICATION_JSON)
    public DominioResponseEntity Caducidad() throws Exception {
        DominioResponseEntity response = new DominioResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            List<DominioEntity> caducidades = negocioInfractor.listadoCaducidad();
            
            response.setDetalle(caducidades);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("observacion")
    @Produces(MediaType.APPLICATION_JSON)
    public DominioResponseEntity Observacion() throws Exception {
        DominioResponseEntity response = new DominioResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            List<DominioEntity> caducidades = negocioInfractor.listadoObservacion();
            
            response.setDetalle(caducidades);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("/uploadAdjunto")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity UploadAdjunto(@Context HttpServletRequest request, InputStream fileInputStream) throws IOException {
        ResponseEntity resp = new ResponseEntity();
        OutputStream out = null;
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        try{
            InputStream is = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
            Properties prop = new Properties();
            prop.load(is);
            
            Date current = new Date();
            String rutaPrincipal = prop.getProperty("pathRepositorio");
            String nombre = request.getHeader("nombre");
            String extension = request.getHeader("extension");
            String infractorId = request.getHeader("infractorId");
            String origen = request.getHeader("origen"); //1 Primera instancia; 2 Segunda Instancia
            
            String pathFile = FechaUtils.getStringFormatFromDate(current, "yyyyMM") + File.separator + FechaUtils.getStringFormatFromDate(current, "dd");
            nombre = nombre + "_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + "." + extension;
            String fileDocumento = rutaPrincipal + File.separator + pathFile + File.separator + nombre;
            
            File fMakedirs = new File(rutaPrincipal + File.separator + pathFile);
            fMakedirs.mkdirs();
            
            out = new FileOutputStream(new File(fileDocumento));
            byte[] buf = new byte[1024];
            int len;
            while ((len = fileInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            
            InfractorEntity infractor = new InfractorEntity();
            infractor.setInfractorId(Integer.valueOf(infractorId));
            infractor.setOrigen(origen);
            
            if (origen.equals("1") || origen.equals("2")) {
                if (origen.equals("1")) {
                    infractor.setPrimeraNombreAdjunto(nombre);
                    infractor.setPrimeraRutaAdjunto(pathFile);
                } else {
                    infractor.setSegundaNombreAdjunto(nombre);
                    infractor.setSegundaRutaAdjunto(pathFile);
                }
            }
            
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            negocioInfractor.mantenimientoInfractorAdjunto(infractor);
            
            resp.setResult_code(200);
            resp.setError_description("");
        } catch(Exception exc){
            resp.setResult_code(500);
            resp.setError_description("No se pudo subir el archivo. Error" + exc);
        } finally {
            try {
                out.close();
                fileInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        resp.setResponse_date(sdf.format(systemDate));
        
        return resp;
    }
    
    @GET
    @Path("/downloadAdjunto")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response DownloadAdjunto(@Context HttpServletRequest request) throws Exception {
        InputStream isProp = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
        Properties prop = new Properties();
        prop.load(isProp);

        String rutaPrincipal = prop.getProperty("pathRepositorio");
        String infractorId = request.getHeader("infractorId");
        String origen = request.getHeader("origen");
        
        InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
        InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
        
        InfractorEntity infractor = negocioInfractor.infractorById(Integer.parseInt(infractorId));
        
        String ruta = origen.equals("1") ? infractor.getPrimeraRutaAdjunto() : infractor.getSegundaRutaAdjunto();
        String nombre = origen.equals("1") ? infractor.getPrimeraNombreAdjunto() : infractor.getSegundaNombreAdjunto();
        String path = rutaPrincipal + File.separator + ruta + File.separator + nombre;
        
        Date current = new Date();
        Response response = null;
        File file = new File(path);
        
        if(file.exists()){
            Response.ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=Adjunto_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss"));
            response = builder.build();
        } else {
            response = Response.status(404).entity("El archivo NO EXISTE").type("text/plain").build();
        }
        
        return response;
    }
    
    @POST
    @Path("eliminarAdjunto")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity EliminarAdjunto(InfractorEntity infractor) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
            InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
            
            infractor.setOrigen(infractor.getOrigen().equals("1") ? "3" : infractor.getOrigen().equals("2") ? "4" : "0");
            
            negocioInfractor.mantenimientoInfractorAdjunto(infractor);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("/ficha")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response Ficha(@Context HttpServletRequest request) throws Exception {
        InputStream isProp = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
        Properties prop = new Properties();
        prop.load(isProp);
        
        Date current = new Date();

        String rutaPrincipal = prop.getProperty("pathRepositorio");
        String infractorId = request.getHeader("infractorId");
        
        String fileHtml = "ficha_riffs_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + ".html";
        String filePdf = "ficha_riffs_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + ".pdf";
        String pathFile = rutaPrincipal + File.separator + "Ficha";
        String path = pathFile + File.separator + fileHtml;
        
        InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
        InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
        
        InfractorEntity infractor = negocioInfractor.infractorById(Integer.parseInt(infractorId));
        List<SancionEntity> sanciones = negocioInfractor.listadoInfractorSanciones(Integer.parseInt(infractorId));
        
        StringBuilder htmlFormato = new StringBuilder();
        
        htmlFormato.append("<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE html><html  xmlns='http://www.w3.org/1999/xhtml'>\n");
        htmlFormato.append("<head><meta http-equiv='content-type' content='text/html; charset=ISO-8859-1'/><meta charset='iso-8859-1'/></head>\n");
        htmlFormato.append("<body style='font-family:Lucida Sans,Tahoma,sans-serif;'>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 14px; width:100%;'>\n");
        htmlFormato.append("    <tr>\n");
        htmlFormato.append("	    <td style='width:20%;'><img src='").append(Constante.URL_IMAGE_PUBlIC_SERVER).append("logo_infractor.png' width='100%' /></td>\n");
        htmlFormato.append("	    <td>&nbsp;</td>\n");
        htmlFormato.append("	    <td style='width:35%;'><img src='").append(Constante.URL_IMAGE_PUBlIC_SERVER).append("logo_ucayali.png' width='100%' /></td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3' style='width:100%; padding-left:20px; font-size: 23px; vertical-align:top; text-align: center; font-weight: bold;'>Ficha<br/>Registro de infractores forestales y de fauna silvestre sancionados</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td width='20%;'>&nbsp;</td>\n");
        htmlFormato.append("		<td colspan='2'>\n");
        htmlFormato.append("                <table border=0 cellpadding=0 cellspacing=0 style='width:100%; font-size: 14px;'>\n");
        htmlFormato.append("		       <tr>\n");
        htmlFormato.append("                       <td style='width: 10%;'>&nbsp;</td>\n");
        htmlFormato.append("                       <td style='background-color: #9DC284; height:30px; font-weight: bold; border: 1px solid black; padding-left: 10px;'>Registro N°</td>\n");
        htmlFormato.append("                       <td style='border: 1px solid black; border-left: 0px; padding-left: 10px;'>").append(infractor.getNroRegistro()).append("</td>\n");
        htmlFormato.append("                       <td style='background-color: #9DC284; font-weight: bold; border: 1px solid black; border-left: 0px; padding-left: 10px;'>Fecha de última modificación</td>\n");
        htmlFormato.append("                       <td style='border: 1px solid black; border-left: 0px; padding-left: 10px;'>").append(infractor.getFechaModificacion()).append("</td>\n");
        htmlFormato.append("                   </tr>\n");
        htmlFormato.append("               </table>\n");
        htmlFormato.append("        </td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='4' style='width:100%; background-color: #9DC284; font-weight: bold; border: 1px solid black; padding:5px;'>DATOS DEL INFRACTOR</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Nombre / Razón Social</td>\n");
        htmlFormato.append("		<td colspan='3' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getNombresRazonSocial()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>DNI / RUC</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getDniRuc()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Domicilio Legal</td>\n");
        htmlFormato.append("		<td style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getDomicilioLegal()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Distrito</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getDistritoNombre()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Provincia</td>\n");
        htmlFormato.append("		<td style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getProvinciaNombre()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #9DC284; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>INFRACCIÓN Y SANCIÓN</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        
        for(int i=0; i < sanciones.size(); i++) {
            htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Datos de la infracción y sanción N° ").append((i + 1)).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Tipo infracción</td>\n");
            htmlFormato.append("		<td colspan='8' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(sanciones.get(i).getTipoInfraccionDes()).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Base Legal</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(sanciones.get(i).getBaseLegal()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Multa Impuesta (UIT)</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(sanciones.get(i).getMulta()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Pagó multa?</td>\n");
            htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:7%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(sanciones.get(i).getFlagMulta().equals(1) ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:8%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(sanciones.get(i).getFlagMulta().equals(0) ? "X" : "").append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("</table>\n");
        }
        
        for(int i=0; i < sanciones.size(); i++) {
            htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td colspan='7' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Medidas Administrativas impuestas N° ").append((i + 1)).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Medida Administrativa</td>\n");
            htmlFormato.append("		<td style='width:45%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(sanciones.get(i).getMedidaAdministrativa()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Cumplió con la medida administrativa?</td>\n");
            htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:7%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(sanciones.get(i).getCumplioMedidaAdm().equals(1) ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:8%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(sanciones.get(i).getCumplioMedidaAdm().equals(0) ? "X" : "").append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("</table>\n");
        }
        
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #9DC284; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>DATOS DEL PAS</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Primera Instancia</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>N° Resolución administrativa</td>\n");
        htmlFormato.append("		<td colspan='8' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getPrimeraNroResolAdm()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Fecha de emisión</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getPrimeraFechaEmision()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Fecha de Notificación</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getPrimeraFechaNotificacion()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Adjunto documento?</td>\n");
        htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
        htmlFormato.append("		<td style='width:7%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(infractor.getPrimeraFlagAdjunto().equals(1) ? "X" : "").append("</td>\n");
        htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
        htmlFormato.append("		<td style='width:8%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(infractor.getPrimeraFlagAdjunto().equals(0) ? "X" : "").append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Segunda Instancia</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>N° Resolución administrativa</td>\n");
        htmlFormato.append("		<td colspan='8' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getSegundaNroResolAdm()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Fecha de emisión</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getSegundaFechaEmision()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Fecha de Notificación</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getSegundaFechaNotificacion()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Adjunto documento?</td>\n");
        htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
        htmlFormato.append("		<td style='width:7%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(infractor.getSegundaFlagAdjunto().equals(1) ? "X" : "").append("</td>\n");
        htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
        htmlFormato.append("		<td style='width:8%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(infractor.getSegundaFlagAdjunto().equals(0) ? "X" : "").append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Ejecución Coactiva</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>N° Resolución Ejecución Coactiva</td>\n");
        htmlFormato.append("		<td colspan='3' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getEjecucionNroResolucion()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Fecha de emisión</td>\n");
        htmlFormato.append("		<td style='width:35%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getEjecucionFechaEmision()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Fecha de Notificación</td>\n");
        htmlFormato.append("		<td style='width:35%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getEjecucionFechaNotificacion()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Coactiva</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>N° Resolución Coactiva</td>\n");
        htmlFormato.append("		<td colspan='3' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getCoactivaNroResolucion()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Fecha de emisión</td>\n");
        htmlFormato.append("		<td style='width:35%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getCoactivaFechaEmision()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Fecha de Notificación</td>\n");
        htmlFormato.append("		<td style='width:35%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(infractor.getCoactivaFechaNotificacion()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        htmlFormato.append("<br/>\n");
        htmlFormato.append("</body>\n");
        
        Response response = null;
        File file = new File(path);
        
        byte[] b = htmlFormato.toString().getBytes("ISO-8859-1");
        
        FileOutputStream fileOuputStream = new FileOutputStream(file);
        fileOuputStream.write(b);
        fileOuputStream.close();
        
        if(file.exists()){
            List<String> instrucciones = new ArrayList<>(15);
            instrucciones.add(prop.getProperty("pathWKHTMLTOPDF"));
            instrucciones.add("--page-size");
            instrucciones.add("A4");
            instrucciones.add("--margin-top");
            instrucciones.add("15mm");
            instrucciones.add("--margin-bottom");
            instrucciones.add("15mm");
            instrucciones.add("--encoding");
            instrucciones.add("UTF-8");
            instrucciones.add(fileHtml);
            instrucciones.add(filePdf);
            
            ProcessBuilder pb = new ProcessBuilder(instrucciones);
            pb.directory(new File(pathFile));
            
            pb.redirectErrorStream(true);
            Process process = pb.start();

            BufferedReader inStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = inStreamReader.readLine();
            while(line != null)
            {
                line = inStreamReader.readLine();
            }
            
            inStreamReader.close();
            
            String pathPdf = pathFile + File.separator + filePdf;
            File objFilePDF = new File(pathPdf);
            
            Response.ResponseBuilder builder = Response.ok(objFilePDF);
            builder.header("Content-Disposition", "attachment; filename=Ficha_RIFFS_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss"));
            response = builder.build();
        } else {
            response = Response.status(404).entity("El reporte no se pudo descargar").type("text/plain").build();
        }
        
        return response;
    }
    
    @GET
    @Path("/reporte")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response Reporte(@Context HttpServletRequest request) throws Exception {
        InputStream isProp = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
        Properties prop = new Properties();
        prop.load(isProp);

        String rutaPrincipal = prop.getProperty("pathRepositorio");
        String buscar = request.getHeader("buscar");
        String desde = request.getHeader("desde");
        String hasta = request.getHeader("hasta");
        
        InfractorDAOSQL daoInfractor = new InfractorDAOSQL();
        InfractorNegocio negocioInfractor = new InfractorNegocio(daoInfractor);
        
        List<InfractorEntity> infractores = negocioInfractor.listadoInfractores(buscar, desde, hasta, 1, 10000);
        Date current = new Date();
        
        String fileName = "infractores_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + ".xls";
        String pathFile = rutaPrincipal + File.separator + "Reporte";
        String path = pathFile + File.separator + fileName;
        
        File fMakedirs = new File(pathFile);
        fMakedirs.mkdirs();
        
        FileWriter fw = new FileWriter(path, true);
        BufferedWriter log = new BufferedWriter(fw);
        
        log.write("<html>\n");
        log.write("    <head>\n");
        log.write("        <title>Reporte</title>\n");
        log.write("        <meta http-equiv='Content-Type' content='application/vnd.ms-excel'>\n");
        log.write("        <meta name='Language' content='es'>\n");
        log.write("    </head>\n");
        log.write("    <body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>\n");
        
        log.write("        <table width='100%' border='0' cellspacing='0' cellpadding='0' style='font-size:1em'>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='8'>&nbsp;</td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='8' style='text-align: center;'><h1><b>Registro de Infractores -  Gobierno Regional de Ucayali</b></h1></td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='8'>&nbsp;</td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='8' style='text-align:center;'><b>Fecha y hora del reporte</b>:" + FechaUtils.getStringFormatFromDate(current, "dd/MM/yyyy HH:mm:ss") + "  - <b>Filtro:</b> " + buscar + "</b></td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='8'>&nbsp;</td>\n");
        log.write("            </tr>\n");
        log.write("        </table>\n");

        log.write("        <table width='100%' border='2' cellspacing='2' cellpadding='0' style='font-size:0.8em'>\n");
        log.write("            <thead>\n");
        log.write("                <tr>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>Numero Registro</th>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>Nombres completos / Razón Social</th>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>DNI/RUC</th>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>Fecha de Alta</th>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>Domicilio Legal</th>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>Departamento</th>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>Provincia</th>\n");
        log.write("                    <th style='background-color:#BFBFBF;'>Distrito</th>\n");
        log.write("                </tr>\n");
        log.write("            </thead>\n");
        log.write("            <tbody>\n");
        
        String bg = "#E2EFDA";
        for(int i=0; i<infractores.size(); i++){
            bg = bg.equals("#FFFFFF") ? "#E2EFDA" : "#FFFFFF";
            log.write("                    <tr>\n<td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getNroRegistro()) + "</td>\n");
            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getNombresRazonSocial()) + "</td>\n");
            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getDniRuc()) + "</td>\n");
            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getFecha()) + "</td>\n");
            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getDomicilioLegal()) + "</td>\n");
            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getDepartamentoNombre()) + "</td>\n");
            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getProvinciaNombre()) + "</td>\n");
            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(infractores.get(i).getDistritoNombre()) + "</td>\n</tr>");
        }
            
        log.write("            </tbody>");
        log.write("        </table>");
        log.write("    </body>");
        log.write("</html>");
        
        log.close();
        fw.close();
        isProp.close();
        
        Response response = null;
        File file = new File(path);
        
        if(file.exists()){
            Response.ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=Reporte_RIFFS_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss"));
            response = builder.build();
        } else {
            response = Response.status(404).entity("El reporte no se pudo descargar").type("text/plain").build();
        }
        
        return response;
    }
    
    @GET
    @Path("/reporteCustodio")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response ReporteCustodio(@Context HttpServletRequest request) throws Exception {
        InputStream isProp = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
        Properties prop = new Properties();
        prop.load(isProp);

        String rutaPrincipal = prop.getProperty("pathRepositorio");
        String buscar = request.getHeader("buscar");
        String tipo = request.getHeader("tipoCustodio");
        
        int totalColumnas = (tipo.equals("01") ? 32 : 27);
        
        CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
        CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
        
        List<CustodioReporteEntity> custodios = negocioCustodio.listadoCustodioReporte(tipo, buscar);
        Date current = new Date();
        
        String fileName = "custodios_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + ".xls";
        String pathFile = rutaPrincipal + File.separator + "Reporte";
        String path = pathFile + File.separator + fileName;
        
        File fMakedirs = new File(pathFile);
        fMakedirs.mkdirs();
        
        FileWriter fw = new FileWriter(path, true);
        BufferedWriter log = new BufferedWriter(fw);
        
        log.write("<html>\n");
        log.write("    <head>\n");
        log.write("        <title>Reporte</title>\n");
        log.write("        <meta http-equiv='Content-Type' content='application/vnd.ms-excel'>\n");
        log.write("        <meta name='Language' content='es'>\n");
        log.write("    </head>\n");
        log.write("    <body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>\n");
        
        log.write("        <table width='100%' border='0' cellspacing='0' cellpadding='0' style='font-size:1em'>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='" + totalColumnas + "'>&nbsp;</td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='" + totalColumnas + "' style='text-align: center;'><h1><b>Registro de Custodios del Patrimonio Forestal y de Fauna Silvestre de Ucayali</b></h1></td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='" + totalColumnas + "'>&nbsp;</td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='" + totalColumnas + "' style='text-align:center;'><b>Fecha y hora del reporte</b>:" + FechaUtils.getStringFormatFromDate(current, "dd/MM/yyyy HH:mm:ss") + "  - <b>Filtro:</b> " + buscar + "</b></td>\n");
        log.write("            </tr>\n");
        log.write("            <tr>\n");
        log.write("                <td colspan='" + totalColumnas + "'>&nbsp;</td>\n");
        log.write("            </tr>\n");
        log.write("        </table>\n");
        
        if(tipo.equals("01")) {
            log.write("        <table width='100%' border='2' cellspacing='2' cellpadding='0' style='font-size:0.8em'>\n");
            log.write("            <thead>\n");
            log.write("                <tr>\n");
            log.write("                    <td colspan='32' style='text-align:center; background-color:#548235; color: white;'><b>CONCESIONARIOS DE TÍTULOS HABILITANTES</b></td>\n");
            log.write("                </tr>\n");
            log.write("                <tr>\n");
            log.write("                    <td colspan='9' rowspan='2' style='text-align:center; background-color:#A9D08E;'><b>DATOS DEL TÍTULO HABILITANTES</b></td>\n");
            log.write("                    <td colspan='23' style='text-align:center; background-color:#A9D08E;'><b>LISTA DE CUSTODIOS </b></td>\n");
            log.write("                </tr>\n");
            log.write("                <tr>\n");
            log.write("                    <td colspan='4' style='text-align:center; background-color:#C6E0B4;'><b>DATOS GENERALES</b></td>\n");
            log.write("                    <td colspan='3' style='text-align:center; background-color:#C6E0B4;'><b>CAPACITACIÓN</b></td>\n");
            log.write("                    <td colspan='7' style='text-align:center; background-color:#C6E0B4;'><b>RESOLUCIÓN DE ACREDITACIÓN</b></td>\n");
            log.write("                    <td colspan='4' style='text-align:center; background-color:#C6E0B4;'><b>PÉRDIDA DE ACREDITACIÓN</b></td>\n");
            log.write("                    <td colspan='5' style='text-align:center; background-color:#C6E0B4;'><b>RENOVACIÓN</b></td>\n");
            log.write("                </tr>\n");
            log.write("                <tr>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NRO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>N° TÍTULO HABILITANTE</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NOMBRE TÍTULO HABILITANTE</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NOMBRE TITULAR TÍTULO HABILITANTE</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>DNI O RUC</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NOMBRE REPRESENTANTE LEGAL</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>TIPO TÍTULO HABILITANTE</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>DISTRITO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>PROVINCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NOMBRE</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>APELLIDOS</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>DNI O CE</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿RECIBIÓ CAPACITACIÓN?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>N° CONSTANCIA CAPACITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA CAPACITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿ADJUNTÓ CONSTANCIA?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA SOLICITUD DE RECONOCIMIENTO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿ADJUNTÓ SOLICITUD?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>ACTO ADMINISTRATIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA DE ACTO ADMINISTRATIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>VIIGENCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>PERIODO DE VIGENCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>CÓDIGO CARNÉ DE ACREDITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿PERDIÓ LA ACREDITACIÓN?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>MOTIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿ADJUNTÓ ACTO ADMINISTRATIVO DE LA PÉRDIDA?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA DE ACTO ADMINISTRATIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿PRESENTÓ RENOVACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>ACTO ADMINISTRATIVO DE LA RENOVACIÓN DE LA ACREDITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA DE ACTO ADMINISTRATIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>VIGENCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>PERIODO DE VIGENCIA</b></td>\n");
            log.write("                </tr>\n");
            log.write("            </thead>\n");
            log.write("            <tbody>\n");

            if (!GeneralUtil.listaEsVacioNulo(custodios)) {
                Integer titularId = custodios.get(0).getTitularId();
                Integer custodioId = custodios.get(0).getCustodioId();

                String bg = "#DDEBF7";
                int fila = 0;
                for(int i = 0;  i< custodios.size(); i++){
                    if(i == 0 || !(titularId.equals(custodios.get(i).getTitularId()) && custodioId.equals(custodios.get(i).getCustodioId()))){
                        titularId = custodios.get(i).getTitularId();
                        custodioId = custodios.get(i).getCustodioId();

                        int idx = i;
                        boolean continuar = true;
                        List<CustodioReporteEntity> renovaciones = new ArrayList();

                        while(continuar && idx < custodios.size()){
                            renovaciones.add(custodios.get(idx));
                            idx = idx + 1;
                            if(idx < custodios.size()) {
                                continuar = titularId.equals(custodios.get(idx).getTitularId()) && custodioId.equals(custodios.get(idx).getCustodioId());
                            } else {
                                continuar = false;
                            }
                        }

                        int rowSpan = renovaciones.size();

                        bg = bg.equals("#D6DCE4") ? "#DDEBF7" : "#D6DCE4";
                        fila = fila + 1;

                        log.write("                    <tr>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + fila + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getNroTituloHabilitante()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getNombreTituloHabilitante()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>&nbsp;</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getDniRucTitular()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getNombreRepLegal()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>&nbsp;</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getDistritoNombre()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getProvinciaNombre()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getNombre()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getApellidos()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getDni()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (custodios.get(i).getCapacitacionFlag().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCapacitacionNumero()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCapacitacionFecha()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (custodios.get(i).getFlagAdjuntoCapacitacion().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCustodioFechaSolicitud()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (custodios.get(i).getFlagAdjuntoReconocimiento().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getActoAdministrativo()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getActoAdministrativoFecha()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + custodios.get(i).getActoAdministrativoVigencia() + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>Del " + GeneralUtil.limpiarCadena(custodios.get(i).getVigenciaDesde()) + " al " + GeneralUtil.limpiarCadena(custodios.get(i).getVigenciaHasta()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCarneCodigo()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (custodios.get(i).getPerdidaFlag().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getPerdidaMotivoDes()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (custodios.get(i).getFlagAdjuntoPerdidaActoAdm().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getPerdidaFecha()) + "</td>\n");
                        log.write("                    <td style='background-color:" + bg + ";'>" + (renovaciones.get(0).getRenovacionFlag().equals(0) ? "NO" : "SI") + "</td>\n");

                        if (renovaciones.get(0).getRenovacionFlag().equals(0)){
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                        } else {
                            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionResolucion()) + "</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionResolucionFecha()) + "</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>" + renovaciones.get(0).getRenovacionResolucionVigencia() + "</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>Del " + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionVigenciaDesde()) + " al " + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionVigenciaHasta()) + "</td>\n");
                        }

                        log.write("                    </tr>\n");

                        for(int j=1; j<renovaciones.size(); j++) {
                            log.write("                    <tr>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>" + (renovaciones.get(j).getRenovacionFlag().equals(0) ? "NO" : "SI") + "</td>\n");

                            if (renovaciones.get(j).getRenovacionFlag().equals(0)){
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            } else {
                                log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionResolucion()) + "</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionResolucionFecha()) + "</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>" + renovaciones.get(j).getRenovacionResolucionVigencia() + "</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>Del " + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionVigenciaDesde()) + " al " + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionVigenciaHasta()) + "</td>\n");
                            }

                            log.write("                    </tr>\n");
                        }
                    }
                }
            }

            log.write("            </tbody>\n");
            log.write("        </table>\n");
        }
        
        if(tipo.equals("02")) {
            log.write("        <table width='100%' border='2' cellspacing='2' cellpadding='0' style='font-size:0.8em'>\n");
            log.write("            <thead>\n");
            log.write("                <tr>\n");
            log.write("                    <td colspan='27' style='text-align:center; background-color:#548235; color: white;'><b>COMITÉ DE VIGILANCIA Y CONTROL</b></td>\n");
            log.write("                </tr>\n");
            log.write("                <tr>\n");
            log.write("                    <td colspan='8' rowspan='2' style='text-align:center; background-color:#A9D08E;'><b>DATOS DE LA COMUNIDAD</b></td>\n");
            log.write("                    <td colspan='19' style='text-align:center; background-color:#A9D08E;'><b>DATOS DEL COMITÉ DE VIGILANCIA Y CONTROL RECONOCIDO</b></td>\n");
            log.write("                </tr>\n");
            log.write("                <tr>\n");
            log.write("                    <td colspan='6' style='text-align:center; background-color:#C6E0B4;'><b>DATOS</b></td>\n");
            log.write("                    <td colspan='8' style='text-align:center; background-color:#C6E0B4;'><b>CUSTODIOS REGISTRADOS</b></td>\n");
            log.write("                    <td colspan='5' style='text-align:center; background-color:#C6E0B4;'><b>RENOVACIÓN</b></td>\n");
            log.write("                </tr>\n");
            log.write("                <tr>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NRO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NOMBRE DE LA COMUNIDAD</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>DISTRITO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>PROVINCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>EXTENSIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿CUENTA CON PERMISO FORESTAL?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>N° PERMISO FORESTAL</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>PERIODO DE VIGENCIA DEL PEMRISO FORESTAL</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA DE SOLICITUD DE RECONOCIMIENTO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿ADJUNTÓ SOLICITUD DE RECONOCIMIENTO?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>ACTO ADMINISTRATIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA DE ACTO ADMINISTRATIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>VIGENCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>PERIODO DE VIGENCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>NOMBRES Y APELLIDOS</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>DNI</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>CARGO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>N° CONSTANCIA DE CAPACITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA DE CAPACITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿ADJUNTÓ CONSTANCIA DE CAPACITACIÓN?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>CÓDIGO DE CARNÉ DE ACREDITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿ADJUNTÓ CARNÉ DE ACREDITACIÓN?</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>¿PRESENTÓ RENOVACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>ACTO ADMINISTRATIVO DE LA RENOVACIÓN DE LA ACREDITACIÓN</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>FECHA DE ACTO ADMINISTRATIVO</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>VIGENCIA</b></td>\n");
            log.write("                    <td style='text-align:center; background-color:#E2EFDA;'><b>PERIODO DE VIGENCIA</b></td>\n");
            log.write("                </tr>\n");
            log.write("            </thead>\n");
            log.write("            <tbody>\n");

            if (!GeneralUtil.listaEsVacioNulo(custodios)) {
                Integer titularId = custodios.get(0).getTitularId();
                Integer custodioId = custodios.get(0).getCustodioId();

                String bg = "#DDEBF7";
                int fila = 0;
                for(int i = 0;  i< custodios.size(); i++){
                    if(i == 0 || !(titularId.equals(custodios.get(i).getTitularId()) && custodioId.equals(custodios.get(i).getCustodioId()))){
                        titularId = custodios.get(i).getTitularId();
                        custodioId = custodios.get(i).getCustodioId();

                        int idx = i;
                        boolean continuar = true;
                        List<CustodioReporteEntity> renovaciones = new ArrayList();

                        while(continuar && idx < custodios.size()){
                            renovaciones.add(custodios.get(idx));
                            idx = idx + 1;
                            if(idx < custodios.size()) {
                                continuar = titularId.equals(custodios.get(idx).getTitularId()) && custodioId.equals(custodios.get(idx).getCustodioId());
                            } else {
                                continuar = false;
                            }
                        }

                        int rowSpan = renovaciones.size();

                        bg = bg.equals("#D6DCE4") ? "#DDEBF7" : "#D6DCE4";
                        fila = fila + 1;

                        log.write("                    <tr>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + fila + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getNombreTitularComunidad()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getDistritoNombre()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getProvinciaNombre()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getExtension()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>&nbsp;</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>&nbsp;</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>&nbsp;</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getFechaSolicitud()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (custodios.get(i).getFlagAdjuntoReconocimiento().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getComiteActoReconocimiento()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getFechaActoReconocimiento()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + custodios.get(i).getVigencia() + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>Del " + GeneralUtil.limpiarCadena(custodios.get(i).getPeriodoDesde()) + " al " + GeneralUtil.limpiarCadena(custodios.get(i).getPeriodoHasta()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getNombre()) + " " + GeneralUtil.limpiarCadena(custodios.get(i).getApellidos()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getDni()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCargo()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCapacitacionNumero()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCapacitacionFecha()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (renovaciones.get(0).getCapacitacionFlag().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(custodios.get(i).getCarneCodigo()) + "</td>\n");
                        log.write("                    <td rowspan = '" + rowSpan + "' style='background-color:" + bg + ";'>" + (renovaciones.get(0).getFlagAdjuntoCarne().equals(0) ? "NO" : "SI") + "</td>\n");
                        log.write("                    <td style='background-color:" + bg + ";'>" + (renovaciones.get(0).getRenovacionFlag().equals(0) ? "NO" : "SI") + "</td>\n");

                        if (renovaciones.get(0).getRenovacionFlag().equals(0)){
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                        } else {
                            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionResolucion()) + "</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionResolucionFecha()) + "</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>" + renovaciones.get(0).getRenovacionResolucionVigencia() + "</td>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>Del " + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionVigenciaDesde()) + " al " + GeneralUtil.limpiarCadena(renovaciones.get(0).getRenovacionVigenciaHasta()) + "</td>\n");
                        }

                        log.write("                    </tr>\n");

                        for(int j=1; j<renovaciones.size(); j++) {
                            log.write("                    <tr>\n");
                            log.write("                    <td style='background-color:" + bg + ";'>" + (renovaciones.get(j).getRenovacionFlag().equals(0) ? "NO" : "SI") + "</td>\n");

                            if (renovaciones.get(j).getRenovacionFlag().equals(0)){
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>&nbsp;</td>\n");
                            } else {
                                log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionResolucion()) + "</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>" + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionResolucionFecha()) + "</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>" + renovaciones.get(j).getRenovacionResolucionVigencia() + "</td>\n");
                                log.write("                    <td style='background-color:" + bg + ";'>Del " + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionVigenciaDesde()) + " al " + GeneralUtil.limpiarCadena(renovaciones.get(j).getRenovacionVigenciaHasta()) + "</td>\n");
                            }

                            log.write("                    </tr>\n");
                        }
                    }
                }
            }

            log.write("            </tbody>\n");
            log.write("        </table>\n");
        }
        
        log.write("    </body>");
        log.write("</html>");
        
        log.close();
        fw.close();
        isProp.close();
        
        Response response = null;
        File file = new File(path);
        
        if(file.exists()){
            Response.ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=Reporte_RIFFS_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss"));
            response = builder.build();
        } else {
            response = Response.status(404).entity("El reporte no se pudo descargar").type("text/plain").build();
        }
        
        return response;
    }
    
    @GET
    @Path("/fichaCustodio")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response FichaCustodio(@Context HttpServletRequest request) throws Exception {
        InputStream isProp = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
        Properties prop = new Properties();
        prop.load(isProp);
        
        Date current = new Date();

        String rutaPrincipal = prop.getProperty("pathRepositorio");
        String titularId = request.getHeader("titularId");
        
        String fileHtml = "ficha_rcffs_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + ".html";
        String filePdf = "ficha_rcffs_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + ".pdf";
        String pathFile = rutaPrincipal + File.separator + "Ficha";
        String path = pathFile + File.separator + fileHtml;
        
        CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
        CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
        
        TitularEntity titular = negocioCustodio.obtenerTitularById(Integer.parseInt(titularId));
        List<CustodioEntity> custodios = negocioCustodio.listadoCustodios(Integer.parseInt(titularId), "", "", "", 1, 1000);
        
        StringBuilder htmlFormato = new StringBuilder();
        
        htmlFormato.append("<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE html><html  xmlns='http://www.w3.org/1999/xhtml'>\n");
        htmlFormato.append("<head><meta http-equiv='content-type' content='text/html; charset=ISO-8859-1'/><meta charset='iso-8859-1'/></head>\n");
        htmlFormato.append("<body style='font-family:Lucida Sans,Tahoma,sans-serif;'>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 14px; width:100%;'>\n");
        htmlFormato.append("    <tr>\n");
        htmlFormato.append("	    <td style='width:20%;'><img src='").append(Constante.URL_IMAGE_PUBlIC_SERVER).append("logo_infractor.png' width='100%' /></td>\n");
        htmlFormato.append("	    <td>&nbsp;</td>\n");
        htmlFormato.append("	    <td style='width:35%;'><img src='").append(Constante.URL_IMAGE_PUBlIC_SERVER).append("logo_ucayali.png' width='100%' /></td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3' style='width:100%; padding-left:20px; font-size: 23px; vertical-align:top; text-align: center; font-weight: bold;'>Ficha<br/>Registro de Custodios del Patrimonio Forestal y de Fauna Silvestre de Ucayali</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='3'>&nbsp;</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='4' style='width:100%; background-color: #5E7E52; font-weight: bold; border: 1px solid black; padding:5px; color: white;'>CONCESIONARIOS DE TÍTULOS HABILITANTES</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td colspan='4' style='width:100%; background-color: #9DC284; font-weight: bold; border: 1px solid black; padding:5px;'>DATOS DEL TÍTULO HABILITANTE</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>N° Titulo Habilitante</td>\n");
        htmlFormato.append("		<td colspan='3' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getNroTituloHabilitante()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Nombre del título Habilitante</td>\n");
        htmlFormato.append("		<td colspan='3' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getNombreTituloHabilitante()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Nombre del títular de título habilitante</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getNombreTituloHabilitante()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>DNI o RUC</td>\n");
        htmlFormato.append("		<td style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getDniRucTitular()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Nombre del representante legal</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getNombreRepLegal()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>DNI</td>\n");
        htmlFormato.append("		<td style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getDniRucRepLegal()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Tipo</td>\n");
        htmlFormato.append("		<td colspan='3' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getTipoCustodioDes()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("	<tr>\n");
        htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Distrito</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getDistritoNombre()).append("</td>\n");
        htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Provincia</td>\n");
        htmlFormato.append("		<td style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(titular.getProvinciaNombre()).append("</td>\n");
        htmlFormato.append("	</tr>\n");
        htmlFormato.append("</table>\n");
        
        for(int i=0; i<custodios.size(); i++) {
            htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #9DC284; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>CUSTODIO ACREDITADO 1</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("</table>\n");
            htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td colspan='7' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Datos Generales</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Nombres</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getNombre()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Apellidos</td>\n");
            htmlFormato.append("		<td colspan='4' style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getApellidos()).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>DNI o CE</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getDni()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Recibió Capacitación?</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:16%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getCapacitacionFlag().equals(1) ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:17%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getCapacitacionFlag().equals(0) ? "X" : "").append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("</table>\n");
            htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td colspan='7' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Capacitación</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>N° Constancia de capacitación</td>\n");
            htmlFormato.append("		<td colspan='6' style='width:85%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getCapacitacionNumero()).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Fecha de capacitación</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getCapacitacionFecha()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Adjuntó constancia?</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:16%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(!custodios.get(i).getCapacitacionNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:17%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getCapacitacionNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("</table>\n");
            htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td colspan='7' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Resolución de acreditación</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Fecha de solicitud de reconocimiento</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getFechaSolicitud()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Adjuntó solicitud de renocimiento?</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:16%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(!custodios.get(i).getReconocimientoNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:17%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getReconocimientoNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Acto administrativo</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getActoAdministrativo()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Fecha de acto administrativo</td>\n");
            htmlFormato.append("		<td colspan='4' style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getActoAdministrativoFecha()).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Vigencia</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getActoAdministrativoVigencia()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Período de vigencia del permiso</td>\n");
            htmlFormato.append("		<td colspan='4' style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>Desde ").append(custodios.get(i).getVigenciaDesde()).append(" hasta ").append(custodios.get(i).getVigenciaHasta()).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Código de carné de acreditación</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getCarneCodigo()).append("</td>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>¿Adjuntó carné de acreditación?</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:16%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(!custodios.get(i).getCarneNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:11%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:17%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getCarneNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("</table>\n");
            htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("	    <td colspan='7' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Pérdida de la acreditación</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px;  padding:5px; font-weight: bold;'>Pérdida de acreditación</td>\n");
            htmlFormato.append("		<td style='width:3%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:4%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getPerdidaFlag().equals(1) ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:3%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getPerdidaFlag().equals(0) ? "X" : "").append("</td>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Motivo</td>\n");
            htmlFormato.append("		<td style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getPerdidaMotivoDes()).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("	<tr>\n");
            htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px;  padding:5px; font-weight: bold;'>¿Adjuntó acto administrativo de pérdida de la acreditación?</td>\n");
            htmlFormato.append("		<td style='width:3%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
            htmlFormato.append("		<td style='width:4%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(!custodios.get(i).getPerdidaActoAdmNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("		<td style='width:3%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
            htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(custodios.get(i).getPerdidaActoAdmNombre().equals("") ? "X" : "").append("</td>\n");
            htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Fecha de acto administrativo</td>\n");
            htmlFormato.append("		<td style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(custodios.get(i).getPerdidaFecha()).append("</td>\n");
            htmlFormato.append("	</tr>\n");
            htmlFormato.append("</table>\n");
            
            List<RenovacionEntity> renovaciones = negocioCustodio.listadoRenovaciones(custodios.get(i).getCustodioId());
            if(!GeneralUtil.listaEsVacioNulo(renovaciones)){
                for(int j=0; j<renovaciones.size(); j++){
                    htmlFormato.append("<table align='center' border='0' cellpadding='0' cellspacing='0' style='font-size: 12px; width:100%;'>\n");
                    htmlFormato.append("	<tr>\n");
                    htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>Renovaciones</td>\n");
                    htmlFormato.append("	</tr>\n");
                    htmlFormato.append("	<tr>\n");
                    htmlFormato.append("	    <td colspan='9' style='width:100%; background-color: #E2EFD9; font-weight: bold; border: 1px solid black; border-top: 0px; padding:5px;'>N° ").append((j + 1)).append("</td>\n");
                    htmlFormato.append("	</tr>\n");
                    htmlFormato.append("	<tr>\n");
                    htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px;  padding:5px; font-weight: bold;'>¿Presentó Renovacion?</td>\n");
                    htmlFormato.append("		<td style='width:3%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>SÍ</td>\n");
                    htmlFormato.append("		<td style='width:4%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(renovaciones.get(j).getFlagRenovacion().equals(1) ? "X" : "").append("</td>\n");
                    htmlFormato.append("		<td style='width:3%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>NO</td>\n");
                    htmlFormato.append("		<td style='width:5%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>").append(renovaciones.get(j).getFlagRenovacion().equals(0) ? "X" : "").append("</td>\n");
                    htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Acto administrativo de la renovación de la acreditación</td>\n");
                    htmlFormato.append("		<td colspan='3'  style='width:55%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(renovaciones.get(j).getResolucion()).append("</td>\n");
                    htmlFormato.append("	</tr>\n");
                    htmlFormato.append("	<tr>\n");
                    htmlFormato.append("	    <td style='width:15%; border: 1px solid black; border-top: 0px; padding:5px; font-weight: bold;'>Fecha Acto administrativo</td>\n");
                    htmlFormato.append("		<td colspan='4' style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(renovaciones.get(j).getResolucionFecha()).append("</td>\n");
                    htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Vigencia</td>\n");
                    htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px;'>").append(renovaciones.get(j).getResolucionVigencia()).append("</td>\n");
                    htmlFormato.append("		<td style='width:15%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; font-weight: bold;'>Periodo de vigencia</td>\n");
                    htmlFormato.append("		<td style='width:25%; border: 1px solid black; border-top: 0px; border-left: 0px; padding:5px; text-align: center;'>Desde ").append(renovaciones.get(j).getVigenciaDesde()).append(" hasta ").append(renovaciones.get(j).getVigenciaHasta()).append("</td>\n");
                    htmlFormato.append("	</tr>\n");
                    htmlFormato.append("</table>\n");
                }
            }
        }
        
        htmlFormato.append("<br/>\n");
        htmlFormato.append("</body>\n");
        
        Response response = null;
        File file = new File(path);
        
        byte[] b = htmlFormato.toString().getBytes("ISO-8859-1");
        
        FileOutputStream fileOuputStream = new FileOutputStream(file);
        fileOuputStream.write(b);
        fileOuputStream.close();
        
        if(file.exists()){
            List<String> instrucciones = new ArrayList<>(15);
            instrucciones.add(prop.getProperty("pathWKHTMLTOPDF"));
            instrucciones.add("--page-size");
            instrucciones.add("A4");
            instrucciones.add("--margin-top");
            instrucciones.add("15mm");
            instrucciones.add("--margin-bottom");
            instrucciones.add("15mm");
            instrucciones.add("--encoding");
            instrucciones.add("UTF-8");
            instrucciones.add(fileHtml);
            instrucciones.add(filePdf);
            
            ProcessBuilder pb = new ProcessBuilder(instrucciones);
            pb.directory(new File(pathFile));
            
            pb.redirectErrorStream(true);
            Process process = pb.start();

            BufferedReader inStreamReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = inStreamReader.readLine();
            while(line != null)
            {
                line = inStreamReader.readLine();
            }
            
            inStreamReader.close();
            
            String pathPdf = pathFile + File.separator + filePdf;
            File objFilePDF = new File(pathPdf);
            
            Response.ResponseBuilder builder = Response.ok(objFilePDF);
            builder.header("Content-Disposition", "attachment; filename=Ficha_RIFFS_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss"));
            response = builder.build();
        } else {
            response = Response.status(404).entity("El reporte no se pudo descargar").type("text/plain").build();
        }
        
        return response;
    }
    
    @GET
    @Path("titulares")
    @Produces(MediaType.APPLICATION_JSON)
    public TitularListadoResponseEntity Titulares(@QueryParam("tipo") String tipo, @QueryParam("buscar") String buscar, @QueryParam("desde") String desde, @QueryParam("hasta") String hasta, @QueryParam("pagina") Integer pagina, @QueryParam("regxpag") Integer regxpag) throws Exception {
        TitularListadoResponseEntity response = new TitularListadoResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            List<TitularEntity> titulares = negocioCustodio.listadoTitulares(tipo, buscar, desde, hasta, pagina, regxpag);
            response.setContent(titulares);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("titulos")
    @Produces(MediaType.APPLICATION_JSON)
    public TituloListadoResponseEntity Titulos(@QueryParam("titularId") Integer titularId) throws Exception {
        TituloListadoResponseEntity response = new TituloListadoResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            List<TituloEntity> titulos = negocioCustodio.listadoTitulos(titularId);
            response.setContent(titulos);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("tiposCustodio")
    @Produces(MediaType.APPLICATION_JSON)
    public DominioResponseEntity TiposCustodio() throws Exception {
        DominioResponseEntity response = new DominioResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            List<DominioEntity> tiposCustodios = negocioCustodio.listadoTipoCustodio();
            
            response.setDetalle(tiposCustodios);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("custodios")
    @Produces(MediaType.APPLICATION_JSON)
    public CustodioListadoResponseEntity Custodios(@QueryParam("titularId") Integer titularId, @QueryParam("buscar") String buscar, @QueryParam("desde") String desde, @QueryParam("hasta") String hasta, @QueryParam("pagina") Integer pagina, @QueryParam("regxpag") Integer regxpag) throws Exception {
        CustodioListadoResponseEntity response = new CustodioListadoResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            List<CustodioEntity> custodios = negocioCustodio.listadoCustodios(titularId, buscar, desde, hasta, pagina, regxpag);
            
            response.setContent(custodios);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("renovaciones")
    @Produces(MediaType.APPLICATION_JSON)
    public RenovacionListadoResponseEntity Renovaciones(@QueryParam("custodioId") Integer custodioId) throws Exception {
        RenovacionListadoResponseEntity response = new RenovacionListadoResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            List<RenovacionEntity> renovaciones = negocioCustodio.listadoRenovaciones(custodioId);
            response.setContent(renovaciones);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("tiposPersona")
    @Produces(MediaType.APPLICATION_JSON)
    public DominioResponseEntity TiposPersona() throws Exception {
        DominioResponseEntity response = new DominioResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            List<DominioEntity> tiposPersona = negocioCustodio.listadoTipoPersona();
            
            response.setDetalle(tiposPersona);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crudTitular")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity CrudTitular(TitularEntity titular) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            Integer titularId = negocioCustodio.mantenimientoTitular(titular);
            response.setCode(titularId);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @GET
    @Path("motivosPerdida")
    @Produces(MediaType.APPLICATION_JSON)
    public DominioResponseEntity MotivosPerdida() throws Exception {
        DominioResponseEntity response = new DominioResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            List<DominioEntity> motivoPerdida = negocioCustodio.listadoMotivosPerdida();
            
            response.setDetalle(motivoPerdida);
            
            return response;
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("/uploadTitular")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity UploadTitular(@Context HttpServletRequest request, InputStream fileInputStream) throws IOException {
        ResponseEntity resp = new ResponseEntity();
        OutputStream out = null;
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        try{
            InputStream is = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
            Properties prop = new Properties();
            prop.load(is);
            
            Date current = new Date();
            String rutaPrincipal = prop.getProperty("pathRepositorio");
            String nombre = request.getHeader("nombre");
            String extension = request.getHeader("extension");
            String titularId = request.getHeader("titularId");
            
            String pathFile = FechaUtils.getStringFormatFromDate(current, "yyyyMM") + File.separator + FechaUtils.getStringFormatFromDate(current, "dd");
            nombre = nombre + "_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + "." + extension;
            String fileDocumento = rutaPrincipal + File.separator + pathFile + File.separator + nombre;
            
            File fMakedirs = new File(rutaPrincipal + File.separator + pathFile);
            fMakedirs.mkdirs();
            
            out = new FileOutputStream(new File(fileDocumento));
            byte[] buf = new byte[1024];
            int len;
            while ((len = fileInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            
            TitularEntity titular = new TitularEntity();
            titular.setTitularId(Integer.valueOf(titularId));
            titular.setNombreAdjunto(nombre);
            titular.setRutaAdjunto(pathFile);
            
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            negocioCustodio.mantenimientoTitularAdjunto(titular);
            
            resp.setResult_code(200);
            resp.setError_description("");
        } catch(Exception exc){
            resp.setResult_code(500);
            resp.setError_description("No se pudo subir el archivo. Error" + exc);
        } finally {
            try {
                out.close();
                fileInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        resp.setResponse_date(sdf.format(systemDate));
        
        return resp;
    }
    
    @GET
    @Path("/downloadAdjuntoTitular")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response DownloadAdjuntoTitular(@Context HttpServletRequest request) throws Exception {
        InputStream isProp = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
        Properties prop = new Properties();
        prop.load(isProp);

        String rutaPrincipal = prop.getProperty("pathRepositorio");
        String titularId = request.getHeader("titularId");
        
        CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
        CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
        
        TitularEntity titular = negocioCustodio.obtenerTitularById(Integer.parseInt(titularId));
        
        String ruta = titular.getRutaAdjunto();
        String nombre = titular.getNombreAdjunto();
        String path = rutaPrincipal + File.separator + ruta + File.separator + nombre;
        
        Date current = new Date();
        Response response = null;
        File file = new File(path);
        
        if(file.exists()){
            Response.ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=Adjunto_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss"));
            response = builder.build();
        } else {
            response = Response.status(404).entity("El archivo NO EXISTE").type("text/plain").build();
        }
        
        return response;
    }
    
    @POST
    @Path("eliminarAdjuntoTitular")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity EliminarAdjuntoTitular(TitularEntity titular) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            titular.setNombreAdjunto("");
            titular.setRutaAdjunto("");
            
            negocioCustodio.mantenimientoTitularAdjunto(titular);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("/uploadAdjuntoCustodio")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseEntity UploadAdjuntoCustodio(@Context HttpServletRequest request, InputStream fileInputStream) throws IOException {
        ResponseEntity resp = new ResponseEntity();
        OutputStream out = null;
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        try{
            InputStream is = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
            Properties prop = new Properties();
            prop.load(is);
            
            Date current = new Date();
            String rutaPrincipal = prop.getProperty("pathRepositorio");
            String nombre = request.getHeader("nombre");
            String extension = request.getHeader("extension");
            String custodioId = request.getHeader("custodioId");
            String origen = request.getHeader("origen");
            
            String pathFile = FechaUtils.getStringFormatFromDate(current, "yyyyMM") + File.separator + FechaUtils.getStringFormatFromDate(current, "dd");
            nombre = nombre + "_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss") + "." + extension;
            String fileDocumento = rutaPrincipal + File.separator + pathFile + File.separator + nombre;
            
            File fMakedirs = new File(rutaPrincipal + File.separator + pathFile);
            fMakedirs.mkdirs();
            
            out = new FileOutputStream(new File(fileDocumento));
            byte[] buf = new byte[1024];
            int len;
            while ((len = fileInputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            
            CustodioEntity custodio = new CustodioEntity();
            custodio.setCustodioId(Integer.valueOf(custodioId));
            custodio.setOrigen(origen);
            
            if (origen.equals("1") || origen.equals("2") || origen.equals("3") || origen.equals("4")) {
                if (origen.equals("1")) {
                    custodio.setCapacitacionNombre(nombre);
                    custodio.setCapacitacionRuta(pathFile);
                } else if (origen.equals("2")) {
                    custodio.setReconocimientoNombre(nombre);
                    custodio.setReconocimientoRuta(pathFile);
                } else if (origen.equals("3")) {
                    custodio.setCarneNombre(nombre);
                    custodio.setCarneRuta(pathFile);
                } else if (origen.equals("4")) {
                    custodio.setPerdidaActoAdmNombre(nombre);
                    custodio.setPerdidaActoAdmRuta(pathFile);
                }
            }
            
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            negocioCustodio.mantenimientoCustodioAdjunto(custodio);
            
            resp.setResult_code(200);
            resp.setError_description("");
        } catch(Exception exc){
            resp.setResult_code(500);
            resp.setError_description("No se pudo subir el archivo. Error" + exc);
        } finally {
            try {
                out.close();
                fileInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        resp.setResponse_date(sdf.format(systemDate));
        
        return resp;
    }
    
    @GET
    @Path("/downloadAdjuntoCustodio")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response DownloadAdjuntoCustodio(@Context HttpServletRequest request) throws Exception {
        InputStream isProp = AdjuntoEntity.class.getClassLoader().getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
        Properties prop = new Properties();
        prop.load(isProp);

        String rutaPrincipal = prop.getProperty("pathRepositorio");
        String custodioId = request.getHeader("custodioId");
        String origen = request.getHeader("origen");
        
        CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
        CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
        
        CustodioEntity custodio = negocioCustodio.obtenerCustodioById(Integer.parseInt(custodioId));
        
        String ruta = origen.equals("1") ? custodio.getCapacitacionRuta() : origen.equals("2") ? custodio.getReconocimientoRuta() : origen.equals("3") ? custodio.getCarneRuta() : origen.equals("4") ? custodio.getPerdidaActoAdmRuta() : "";
        String nombre = origen.equals("1") ? custodio.getCapacitacionNombre(): origen.equals("2") ? custodio.getReconocimientoNombre() : origen.equals("3") ? custodio.getCarneNombre() : origen.equals("4") ? custodio.getPerdidaActoAdmNombre() : "";
        String path = rutaPrincipal + File.separator + ruta + File.separator + nombre;
        
        Date current = new Date();
        Response response = null;
        File file = new File(path);
        
        if(file.exists()){
            Response.ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=Adjunto_" + FechaUtils.getStringFormatFromDate(current, "yyyyMMddHHmmss"));
            response = builder.build();
        } else {
            response = Response.status(404).entity("El archivo NO EXISTE").type("text/plain").build();
        }
        
        return response;
    }
    
    @POST
    @Path("eliminarAdjuntoCustodio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity EliminarAdjuntoCustodio(CustodioEntity custodio) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            custodio.setOrigen(custodio.getOrigen().equals("1") ? "5" : custodio.getOrigen().equals("2") ? "6" : custodio.getOrigen().equals("3") ? "7" : custodio.getOrigen().equals("4") ? "8" : "0");
            
            negocioCustodio.mantenimientoCustodioAdjunto(custodio);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crudTitulo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity CrudTitulo(TituloEntity titulo) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            negocioCustodio.mantenimientoTitulo(titulo);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crudCustodio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity CrudCustoodio(CustodioEntity custodio) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            Integer custodioId = negocioCustodio.mantenimientoCustodio(custodio);
            
            response.setCode(custodioId);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    @POST
    @Path("crudRenovacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity CrudRenovacion(RenovacionEntity renovacion) throws Exception {
        ResponseEntity response = new ResponseEntity();
        response.setResult_code(200);
        response.setError_description("");
        
        Date systemDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        
        response.setResponse_date(sdf.format(systemDate));
        
        try {
            CustodioDAOSQL daoCustodio = new CustodioDAOSQL();
            CustodioNegocio negocioCustodio = new CustodioNegocio(daoCustodio);
            
            negocioCustodio.mantenimientoRenovacion(renovacion);
        } catch (Exception exc) {
            response.setResult_code(Constante.ERROR_SERVIDOR);
            response.setError_description(Constante.DESCRIPCION_ERROR_SERVIDOR + ": " + exc.toString());
        }
        
        return response;
    }
    
    public void delay(){
        try{
            int min = 1;
            int max = 999;
            int milli_seconds = 0;
            
            milli_seconds = (int)(Math.random() * (max - min + 1) + min);
            Thread.sleep(milli_seconds);
            
            milli_seconds = (int)(Math.random() * (max - min + 1) + min);
            Thread.sleep(milli_seconds);
            
            milli_seconds = (int)(Math.random() * (max - min + 1) + min);
            Thread.sleep(milli_seconds);
        } catch(Exception exc){
            
        }
    }
    
    public void crearRutas(String fullMethodClass, Date systemDate){
        try{
            //sw obtener ruta LOG / JSON
            
            //sw Obtener ruta JSON Desde CONFIG
            InputStream is = null;
            Properties prop = null;
            try {
                //sw Obtener ruta
                is = Log.class.getResourceAsStream("/net/infractores/mybatis/config/configurationSQL.properties");
                prop = new Properties();
                prop.load(is);

                rutaJSONData = prop.getProperty("logPath");
                rutaLOG = rutaJSONData;
            } catch (Exception ex) {
                System.out.println(fullMethodClass + " - Error al leer el archivo CONFIG - Detalles: " + ex.getMessage());
            } finally {
                if (prop != null) {
                    prop = null;
                }
                if (is != null) {
                    is = null;
                }
            }
            
            //sw Estructura de carpetas
            //sw LOG = RUTA_LOG/LOG/YYYYMM/iSleep_RestService_FECHAREGISTRO.json
            //sw Para el caso de DataDevice = RUTA_LOG/JSON/YYYYMM/DD/HHmmss/dataDevice_DEVICEID_FECHAREGISTRO.json
            SimpleDateFormat sdfDinamico = null;
            File f = null;
            try {
                //sw LOG
                sdfDinamico = new SimpleDateFormat("yyyyMM");
                rutaLOG = rutaLOG + "log" + File.separator + sdfDinamico.format(systemDate);
                
                sdfDinamico = new SimpleDateFormat("dd");
                rutaLOG = rutaLOG + File.separator + sdfDinamico.format(systemDate) + File.separator;

                f = new File(rutaLOG);
                boolean resultadoCreacion = true;
                if (f.exists() == false) {
                    resultadoCreacion = f.mkdirs();
                    if (!resultadoCreacion) {
                        System.out.println(fullMethodClass + " - Error al crear la estructura de carpetas (1)");
                    }
                    f = null;
                }
                
                //sw JSON
                sdfDinamico = new SimpleDateFormat("yyyyMM");
                rutaJSONData = rutaJSONData + "json" + File.separator + sdfDinamico.format(systemDate);

                sdfDinamico.applyPattern("dd");
                rutaJSONData = rutaJSONData + File.separator + sdfDinamico.format(systemDate);

                sdfDinamico.applyPattern("HHmmss");
                rutaJSONData = rutaJSONData + File.separator + sdfDinamico.format(systemDate);

                f = new File(rutaJSONData);
                if (f.exists() == false) {
                    resultadoCreacion = f.mkdirs();
                    if (!resultadoCreacion) {
                        System.out.println(fullMethodClass + " - Error al crear la estructura de carpetas (1)");
                    }
                }
            } catch (Exception ex) {
                System.out.println(fullMethodClass + " - Error al crear la estructura de carpetas (2) - Detalles: " + ex.getMessage());
            } finally {
                if (f != null) {
                    f = null;
                }
                if (sdfDinamico != null) {
                    sdfDinamico = null;
                }
            }
            //sw obtener ruta JSON
        } catch(Exception exc){
            
        }
    }
    
    private boolean sePermiteElConsumoDelWS(String dispositivoDeConsulta) {
        boolean bSePermiteElConsumoDelWS = false;
        if (dispositivoDeConsulta != null) {
            //sw ANDROID: Consultas naticas Android
            //sw OKHHTP: Consumo web usando librerpia okhhtp (https://square.github.io/okhttp/)
            if (dispositivoDeConsulta.trim().toUpperCase().contains("ANDROID")
                    || dispositivoDeConsulta.trim().toUpperCase().contains("OKHTTP")) {
                bSePermiteElConsumoDelWS = true;
            }
        }
        
        return bSePermiteElConsumoDelWS;
    }
    
    public static String nextToken() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        
        return new String(buf);
    }
}