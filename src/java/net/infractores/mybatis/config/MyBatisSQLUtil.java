package net.infractores.mybatis.config;

import java.io.Reader;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MyBatisSQLUtil {

    protected static final SqlSessionFactory SQL_SESSION_FACTORY;

    static {
        try {
            Reader reader = Resources.getResourceAsReader("net/infractores/mybatis/config/SQLMapConfig.xml");

            SQL_SESSION_FACTORY = new SqlSessionFactoryBuilder().build(reader);

        } catch (Exception e) {
            throw new RuntimeException("Error: " + e);
        }
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        return SQL_SESSION_FACTORY;
    }
}