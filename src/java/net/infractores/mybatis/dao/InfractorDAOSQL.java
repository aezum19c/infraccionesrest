package net.infractores.mybatis.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.infractores.entity.AdjuntoEntity;
import net.infractores.entity.DominioEntity;
import net.infractores.entity.InfractorEntity;
import net.infractores.entity.SancionEntity;
import net.infractores.entity.UbigeoEntity;
import net.infractores.mybatis.config.MyBatisSQLUtil;
import net.infractores.mybatis.interfaces.InfractorInterfaceDAO;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

@SuppressWarnings({ "FieldMayBeFinal", "Convert2Diamond", "ConvertToTryWithResources" })
public class InfractorDAOSQL implements InfractorInterfaceDAO {
    private SqlSessionFactory sessionFactory = MyBatisSQLUtil.getSqlSessionFactory();
    
    public InfractorDAOSQL() {}
    
    @Override
    public List<InfractorEntity> listadoInfractores(String buscar, String desde, String hasta, Integer pagina, Integer regxpag) {
        List<InfractorEntity> infractores = new ArrayList<InfractorEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pBuscar", buscar);
            param.put("pDesde", desde);
            param.put("pHasta", hasta);
            param.put("pPagina", pagina);
            param.put("pRegxPagina", regxpag);
            
            infractores = session.selectList("InfractorSQL.listadoInfractores", param);
        }catch(Exception ex){
            infractores = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return infractores;
    }
    
    @Override
    public InfractorEntity infractorById(Integer infractorId) {
        InfractorEntity infractor = new InfractorEntity();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pInfractorId", infractorId);
            
            infractor = session.selectOne("InfractorSQL.infractorById", param);
        }catch(Exception ex){
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return infractor;
    }
    
    @Override
    public List<UbigeoEntity> listadoUbigeos(String tipo, String departamento, String provincia) {
        List<UbigeoEntity> ubigeos = new ArrayList<UbigeoEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pTipo", tipo);
            param.put("pDepartamento", departamento);
            param.put("pProvincia", provincia);
            
            ubigeos = session.selectList("InfractorSQL.listadoUbigeos", param);
        }catch(Exception ex){
            ubigeos = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return ubigeos;
    }
    
    @Override
    public List<SancionEntity> listadoInfractorSanciones(Integer infractorId) {
        List<SancionEntity> sanciones = new ArrayList<SancionEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pInfractorId", infractorId);
            
            sanciones = session.selectList("InfractorSQL.listadoInfractorSanciones", param);
        }catch(Exception ex){
            sanciones = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return sanciones;
    }
    
    @Override
    public List<AdjuntoEntity> listadoInfractorAdjuntos(Integer infractorId, Integer pagina, Integer regxpag) {
        List<AdjuntoEntity> adjuntos = new ArrayList<AdjuntoEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pInfractorId", infractorId);
            param.put("pPagina", pagina);
            param.put("pRegxPagina", regxpag);
            
            adjuntos = session.selectList("InfractorSQL.listadoInfractorAdjuntos", param);
        }catch(Exception ex){
            adjuntos = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return adjuntos;
    }
    
    @Override
    public AdjuntoEntity obtenerInfractorAdjunto(Integer infractorId, Integer adjuntoId) {
        AdjuntoEntity adjunto = new AdjuntoEntity();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pInfractorId", infractorId);
            param.put("pAdjuntoId", adjuntoId);
            
            adjunto = session.selectOne("InfractorSQL.obtenerInfractorAdjunto", param);
        }catch(Exception ex){
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return adjunto;
    }
    
    @Override
    public List<DominioEntity> listadoInfracciones() {
        List<DominioEntity> caducidades = new ArrayList<DominioEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            caducidades = session.selectList("InfractorSQL.listadoInfracciones");
        }catch(Exception ex){
            caducidades = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return caducidades;
    }
    
    @Override
    public List<DominioEntity> listadoCaducidad() {
        List<DominioEntity> caducidades = new ArrayList<DominioEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            caducidades = session.selectList("InfractorSQL.listadoCaducidad");
        }catch(Exception ex){
            caducidades = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return caducidades;
    }
    
    @Override
    public List<DominioEntity> listadoObservacion() {
        List<DominioEntity> observaciones = new ArrayList<DominioEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            observaciones = session.selectList("InfractorSQL.listadoObservacion");
        }catch(Exception ex){
            observaciones = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return observaciones;
    }
    
    @Override
    public Integer crudInfractorPrincipal(InfractorEntity infractor) {
        Integer infractorId = 0;
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pAccion", infractor.getAccion());
            params.put("pInfractorId", infractor.getInfractorId());
            params.put("pNombresRazonSocial", infractor.getNombresRazonSocial());
            params.put("pDniRuc", infractor.getDniRuc());
            params.put("pFecha", infractor.getFecha());
            params.put("pDomicilioLegal", infractor.getDomicilioLegal());
            params.put("pUbigeoId", infractor.getUbigeoId());
            params.put("pEstado", infractor.getEstado());
            params.put("pUsuarioRegistro", infractor.getUsuarioRegistro());
                        
            //session.insert("InfractorSQL.crudInfractorPrincipal", params);
            infractorId = (Integer)session.selectOne("InfractorSQL.crudInfractorPrincipal", params);
            //session.commit();
        } catch (Exception ex) {
            System.out.println("crudInfractorPrincipal.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
        
        return infractorId;
    }
    
    @Override
    public void crudInfractorSecundario(InfractorEntity infractor) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pInfractorId", infractor.getInfractorId());
            params.put("pPrimeraNroResolAdm", infractor.getPrimeraNroResolAdm());
            params.put("pPrimeraFechaEmision", infractor.getPrimeraFechaEmision());
            params.put("pPrimeraFechaNotificacion", infractor.getPrimeraFechaNotificacion());
            params.put("pPrimeraFlagAdjunto", infractor.getPrimeraFlagAdjunto());
            params.put("pSegundaNroResolAdm", infractor.getSegundaNroResolAdm());
            params.put("pSegundaFechaEmision", infractor.getSegundaFechaEmision());
            params.put("pSegundaFechaNotificacion", infractor.getSegundaFechaNotificacion());
            params.put("pSegundaFlagAdjunto", infractor.getSegundaFlagAdjunto());
            params.put("pEjecucionNroResolucion", infractor.getEjecucionNroResolucion());
            params.put("pEjecucionFechaEmision", infractor.getEjecucionFechaEmision());
            params.put("pEjecucionFechaNotificacion", infractor.getEjecucionFechaNotificacion());
            params.put("pCoactivaNroResolucion", infractor.getCoactivaNroResolucion());
            params.put("pCoactivaFechaEmision", infractor.getCoactivaFechaEmision());
            params.put("pCoactivaFechaNotificacion", infractor.getCoactivaFechaNotificacion());
                        
            session.insert("InfractorSQL.crudInfractorSecundario", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("crudInfractorSecundario.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
    
    @Override
    public void mantenimientoInfractor(String accion, InfractorEntity infractor) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pAccion", accion);
            params.put("pInfractorId", infractor.getInfractorId());
            params.put("pNroRegistro", infractor.getNroRegistro());
            params.put("pNombresRazonSocial", infractor.getNombresRazonSocial());
            params.put("pDniRuc", infractor.getDniRuc());
            params.put("pFecha", infractor.getFecha());
            /*params.put("pNroExpediente", infractor.getNroExpediente());
            params.put("pNroActaIntervencion", infractor.getNroActaIntervencion());
            params.put("pNroResolucionInicio", infractor.getNroResolucionInicio());
            params.put("pInfraccionesImputadas", infractor.getInfraccionesImputadas());
            params.put("pInformeInstruccion", infractor.getInformeInstruccion());
            params.put("pNroResolucionDirectoral", infractor.getNroResolucionDirectoral());
            params.put("pInfraccionesAcreditadas", infractor.getInfraccionesAcreditadas());
            params.put("pCaducidad", infractor.getCaducidad());
            params.put("pMulta", infractor.getMulta());
            params.put("pVehiculosInmovilizados", infractor.getVehiculosInmovilizados());
            params.put("pEspecieDecomisadaVolumen", infractor.getEspecieDecomisadaVolumen());
            params.put("pObservacion", infractor.getObservacion());
            params.put("pComentarios", infractor.getComentarios());*/
            params.put("pEstado", infractor.getEstado());
            params.put("pUsuarioRegistro", infractor.getUsuarioRegistro());
                        
            session.insert("InfractorSQL.mantenimientoInfractor", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("mantenmientoInfractor.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
    
    @Override
    public void mantenimientoInfractorAdjunto(InfractorEntity infractor) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pInfractorId", infractor.getInfractorId());
            params.put("pOrigen", infractor.getOrigen());
            params.put("pPrimeraNombreAdjunto", infractor.getPrimeraNombreAdjunto());
            params.put("pPrimeraRutaAdjunto", infractor.getPrimeraRutaAdjunto());
            params.put("pSegundaNombreAdjunto", infractor.getSegundaNombreAdjunto());
            params.put("pSegundaRutaAdjunto", infractor.getSegundaRutaAdjunto());
                        
            session.insert("InfractorSQL.mantenimientoInfractorAdjunto", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("mantenimientoInfractorAdjunto.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
    
    @Override
    public void mantenimientoInfractorSancion(SancionEntity sancion) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pAccion", sancion.getAccion());
            params.put("pInfractorId", sancion.getInfractorId());
            params.put("pSecuenciaId", sancion.getSecuenciaId());
            params.put("pTipoInfraccion", sancion.getTipoInfraccion());
            params.put("pBaseLegal", sancion.getBaseLegal());
            params.put("pMulta", sancion.getMulta());
            params.put("pFlagMulta", sancion.getFlagMulta());
            params.put("pMedidaAdministrativa", sancion.getMedidaAdministrativa());
            params.put("pCumplioMedidaAdm", sancion.getCumplioMedidaAdm());
            params.put("pEstado", sancion.getEstado());
            params.put("pUsuarioRegistro", sancion.getUsuarioRegistro());
                        
            session.insert("InfractorSQL.mantenimientoInfractorSancion", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("mantenimientoInfractorSancion.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
}
