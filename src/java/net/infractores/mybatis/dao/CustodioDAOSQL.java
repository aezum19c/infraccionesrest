package net.infractores.mybatis.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.infractores.entity.CustodioEntity;
import net.infractores.entity.CustodioReporteEntity;
import net.infractores.entity.DominioEntity;
import net.infractores.entity.RenovacionEntity;
import net.infractores.entity.TitularEntity;
import net.infractores.entity.TituloEntity;
import net.infractores.mybatis.config.MyBatisSQLUtil;
import net.infractores.mybatis.interfaces.CustodioInterfaceDAO;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

@SuppressWarnings({ "FieldMayBeFinal", "Convert2Diamond", "ConvertToTryWithResources" })
public class CustodioDAOSQL implements CustodioInterfaceDAO {
    private SqlSessionFactory sessionFactory = MyBatisSQLUtil.getSqlSessionFactory();
    
    public CustodioDAOSQL() {}
    
    @Override
    public List<TitularEntity> listadoTitulares(String tipo, String buscar, String desde, String hasta, Integer pagina, Integer regxpag){
        List<TitularEntity> titulares = new ArrayList<TitularEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pTipo", tipo);
            param.put("pBuscar", buscar);
            param.put("pDesde", desde);
            param.put("pHasta", hasta);
            param.put("pPagina", pagina);
            param.put("pRegxPagina", regxpag);
            
            titulares = session.selectList("CustodioSQL.listadoTitulares", param);
        }catch(Exception ex){
            titulares = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return titulares;
    }
    
    @Override
    public List<TituloEntity> listadoTitulos(Integer titularId){
        List<TituloEntity> titulos = new ArrayList<TituloEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pTitularId", titularId);
            
            titulos = session.selectList("CustodioSQL.listadoTitulos", param);
        }catch(Exception ex){
            titulos = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return titulos;
    }
    
    @Override
    public List<CustodioEntity> listadoCustodios(Integer titularId, String buscar, String desde, String hasta, Integer pagina, Integer regxpag){
        List<CustodioEntity> titulares = new ArrayList<CustodioEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap();
            param.put("pTitularId", titularId);
            param.put("pBuscar", buscar);
            param.put("pDesde", desde);
            param.put("pHasta", hasta);
            param.put("pPagina", pagina);
            param.put("pRegxPagina", regxpag);
            
            titulares = session.selectList("CustodioSQL.listadoCustodios", param);
        }catch(Exception ex){
            titulares = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return titulares;
    }
    
    @Override
    public List<RenovacionEntity> listadoRenovaciones(Integer custodioId){
        List<RenovacionEntity> renovaciones = new ArrayList<RenovacionEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap(); 
            param.put("pCustodioId", custodioId);
            
            renovaciones = session.selectList("CustodioSQL.listadoRenovaciones", param);
        }catch(Exception ex){
            renovaciones = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return renovaciones;
    }
    
    @Override
    public List<CustodioReporteEntity> listadoCustodioReporte(String tipo, String buscar){
        List<CustodioReporteEntity> reporte = new ArrayList<CustodioReporteEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map param = new HashMap();
            param.put("pTipo", tipo);
            param.put("pBuscar", buscar);
            
            reporte = session.selectList("CustodioSQL.listadoCustodioReporte", param);
        }catch(Exception ex){
            reporte = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return reporte;
    }
    
    @Override
    public List<DominioEntity> listadoTipoPersona() {
        List<DominioEntity> tiposPersona = new ArrayList<DominioEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            tiposPersona = session.selectList("CustodioSQL.listadoTipoPersona");
        }catch(Exception ex){
            tiposPersona = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return tiposPersona;
    }
    
    @Override
    public List<DominioEntity> listadoTipoCustodio() {
        List<DominioEntity> tiposCustodio = new ArrayList<DominioEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            tiposCustodio = session.selectList("CustodioSQL.listadoTipoCustodio");
        }catch(Exception ex){
            tiposCustodio = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return tiposCustodio;
    }
    
    @Override
    public List<DominioEntity> listadoMotivosPerdida() {
        List<DominioEntity> motivoPerdida = new ArrayList<DominioEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            motivoPerdida = session.selectList("CustodioSQL.listadoMotivosPerdida");
        }catch(Exception ex){
            motivoPerdida = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return motivoPerdida;
    }
    
    @Override
    public TitularEntity obtenerTitularById(Integer titularId) {
        TitularEntity titularEntity = new TitularEntity();
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pTitularId", titularId);
            
            titularEntity = (TitularEntity)session.selectOne("CustodioSQL.titularById", params);
        } catch (Exception ex) {
            System.out.println("obtenerTitularById.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
        
        return titularEntity;
    }
    
    @Override
    public CustodioEntity obtenerCustodioById(Integer custodioId) {
        CustodioEntity custodioEntity = new CustodioEntity();
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pCustodioId", custodioId);
            
            custodioEntity = (CustodioEntity)session.selectOne("CustodioSQL.custodioById", params);
        } catch (Exception ex) {
            System.out.println("obtenerCustodioById.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
        
        return custodioEntity;
    }
    
    @Override
    public Integer mantenimientoTitular(TitularEntity titular) {
        Integer titularId = 0;
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pAccion", titular.getAccion());
            params.put("pTitularId", titular.getTitularId());
            params.put("pTipoCustodio", titular.getTipoCustodio());
            params.put("pNroTituloHabilitante", titular.getNroTituloHabilitante());
            params.put("pVigenciaInicio", titular.getVigenciaInicio());
            params.put("pVigenciaFin", titular.getVigenciaFin());
            params.put("pNombreTituloHabilitante", titular.getNombreTituloHabilitante());
            params.put("pTipoPersona", titular.getTipoPersona());
            params.put("pNombreTitularComunidad", titular.getNombreTitularComunidad());
            params.put("pDniRucTitular", titular.getDniRucTitular());
            params.put("pNombreRepLegal", titular.getNombreRepLegal());
            params.put("pDniRucRepLegal", titular.getDniRucRepLegal());
            params.put("pAmbitoTerritorial", titular.getAmbitoTerritorial());
            params.put("pUbigeoId", titular.getUbigeoId());
            params.put("pExtension", titular.getExtension());
            params.put("pComiteActoReconocimiento", titular.getComiteActoReconocimiento());
            params.put("pFechaActoReconocimiento", titular.getFechaActoReconocimiento());
            params.put("pVigencia", titular.getVigencia());
            params.put("pPeriodoDesde", titular.getPeriodoDesde());
            params.put("pPeriodoHasta", titular.getPeriodoHasta());
            params.put("pFechaSolicitud", titular.getFechaSolicitud());
            params.put("pEstado", titular.getEstado());
            params.put("pUsuarioRegistro", titular.getUsuarioRegistro());
            
            titularId = (Integer)session.selectOne("CustodioSQL.mantenimientoTitular", params);
        } catch (Exception ex) {
            System.out.println("mantenimientoTitular.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
        
        return titularId;
    }
    
    @Override
    public void mantenimientoTitularAdjunto(TitularEntity titular) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pTitularId", titular.getTitularId());
            params.put("pNombreAdjunto", titular.getNombreAdjunto());
            params.put("pRutaAdjunto", titular.getRutaAdjunto());
                        
            session.insert("CustodioSQL.mantenimientoTitularAdjunto", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("mantenimientoTitularAdjunto.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
    
    @Override
    public void mantenimientoTitulo(TituloEntity titulo) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pAccion", titulo.getAccion());
            params.put("pTitularId", titulo.getTitularId());
            params.put("pSecuenciaId", titulo.getSecuenciaId());
            params.put("pNroTituloHabilitante", titulo.getNroTituloHabilitante());
            params.put("pVigenciaInicio", titulo.getVigenciaInicio());
            params.put("pVigenciaFin", titulo.getVigenciaFin());
            params.put("pEstado", titulo.getEstado());
            params.put("pUsuarioRegistro", titulo.getUsuarioRegistro());
                        
            session.insert("CustodioSQL.mantenimientoTitulo", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("mantenimientoTitulo.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
    
    @Override
    public Integer mantenimientoCustodio(CustodioEntity custodio) {
        Integer custodioId = 0;
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pAccion", custodio.getAccion());
            params.put("pCustodioId", custodio.getCustodioId());
            params.put("pTitularId", custodio.getTitularId());
            params.put("pNombre", custodio.getNombre());
            params.put("pApellidos", custodio.getApellidos());
            params.put("pDni", custodio.getDni());
            params.put("pCargo", custodio.getCargo());
            params.put("pCapacitacionFlag", custodio.getCapacitacionFlag());
            params.put("pCapacitacionFecha", custodio.getCapacitacionFecha());
            params.put("pCapacitacionNumero", custodio.getCapacitacionNumero());
            params.put("pFechaSolicitud", custodio.getFechaSolicitud());
            params.put("pActoAdministrativo", custodio.getActoAdministrativo());
            params.put("pActoAdministrativoFecha", custodio.getActoAdministrativoFecha());
            params.put("pActoAdministrativoVigencia", custodio.getActoAdministrativoVigencia());
            params.put("pVigenciaDesde", custodio.getVigenciaDesde());
            params.put("pVigenciaHasta", custodio.getVigenciaHasta());
            params.put("pCarneCodigo", custodio.getCarneCodigo());
            params.put("pPerdidaFlag", custodio.getPerdidaFlag());
            params.put("pPerdidaMotivo", custodio.getPerdidaMotivo());
            params.put("pPerdidaFecha", custodio.getPerdidaFecha());
            params.put("pEstado", custodio.getEstado());
            params.put("pUsuarioRegistro", custodio.getUsuarioRegistro());
            
            custodioId = (Integer)session.selectOne("CustodioSQL.mantenimientoCustodio", params);
        } catch (Exception ex) {
            System.out.println("mantenimientoCustodio.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
        
        return custodioId;
    }
    
    @Override
    public void mantenimientoCustodioAdjunto(CustodioEntity custodio) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pCustodioId", custodio.getCustodioId());
            params.put("pOrigen", custodio.getOrigen());
            params.put("pCapacitacionNombre", custodio.getCapacitacionNombre());
            params.put("pCapacitacionRuta", custodio.getCapacitacionRuta());
            params.put("pReconocimientoNombre", custodio.getReconocimientoNombre());
            params.put("pReconocimientoRuta", custodio.getReconocimientoRuta());
            params.put("pCarneNombre", custodio.getCarneNombre());
            params.put("pCarneRuta", custodio.getCarneRuta());
            params.put("pPerdidaActoAdmNombre", custodio.getPerdidaActoAdmNombre());
            params.put("pPerdidaActoAdmRuta", custodio.getPerdidaActoAdmRuta());
                        
            session.insert("CustodioSQL.mantenimientoCustodioAdjunto", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("mantenimientoCustodioAdjunto.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
    
    @Override
    public void mantenimientoRenovacion(RenovacionEntity renovacion) {
        SqlSession session = sessionFactory.openSession();
        
        try {
            Map params = new HashMap();
            params.put("pAccion", renovacion.getAccion());
            params.put("pCustodioId", renovacion.getCustodioId());
            params.put("pSecuenciaId", renovacion.getSecuenciaId());
            params.put("pFlagRenovacion", renovacion.getFlagRenovacion());
            params.put("pResolucion", renovacion.getResolucion());
            params.put("pResolucionFecha", renovacion.getResolucionFecha());
            params.put("pResolucionVigencia", renovacion.getResolucionVigencia());
            params.put("pVigenciaDesde", renovacion.getVigenciaDesde());
            params.put("pVigenciaHasta", renovacion.getVigenciaHasta());
            params.put("pEstado", renovacion.getEstado());
            params.put("pUsuarioRegistro", renovacion.getUsuarioRegistro());
                        
            session.insert("CustodioSQL.mantenimientoRenovacion", params);
            session.commit();
        } catch (Exception ex) {
            System.out.println("mantenimientoRenovacion.error = " + ex.getMessage());throw ex;
        } finally{
            session.close();
        }
    }
}
