package net.infractores.mybatis.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.infractores.entity.RolEntity;
import net.infractores.entity.UsuarioEntity;
import net.infractores.mybatis.config.MyBatisSQLUtil;
import net.infractores.mybatis.interfaces.UsuarioInteraceDAO;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

@SuppressWarnings({ "FieldMayBeFinal", "Convert2Diamond", "ConvertToTryWithResources" })
public class UsuarioDAOSQL implements UsuarioInteraceDAO {
    private SqlSessionFactory sessionFactory = MyBatisSQLUtil.getSqlSessionFactory();
    
    public UsuarioDAOSQL(){}
    
    @Override
    public UsuarioEntity validarUsuario(String usuario, String clave) {
        UsuarioEntity user = new UsuarioEntity();
        SqlSession session = sessionFactory.openSession();
        try{
            Map<String, String> userPass = new HashMap<String, String>(); 
            userPass.put("usuario", usuario);
            userPass.put("clave", clave);
            user = (UsuarioEntity)session.selectOne("UsuarioSQL.validarUsuario", userPass);
        }catch(Exception ex){
            user = null;
            System.out.println("" + ex.getMessage());
        }finally{
            session.close();
        }
        
        return user;
    }

    @Override
    public List<RolEntity> rolesUsuario(Integer usuarioId){
        List<RolEntity> roles = new ArrayList<RolEntity>();
        SqlSession session = sessionFactory.openSession();
        try{
            Map<String, Integer> paramUserId = new HashMap<String, Integer>(); 
            paramUserId.put("usuarioId", usuarioId);
            
            roles = session.selectList("UsuarioSQL.rolesUsuario", paramUserId);
        }catch(Exception ex){
            roles = null;
            System.out.println(""+ex.getMessage());
        }finally{
            session.close();
        }
        
        return roles;
    }
}
