package net.infractores.mybatis.interfaces;

import java.util.List;
import net.infractores.entity.RolEntity;
import net.infractores.entity.UsuarioEntity;

public interface UsuarioInterfaceNegocio {
    UsuarioEntity validarUsuario(String usuario, String clave);
    List<RolEntity> rolesUsuario(Integer usuarioId);
}
