package net.infractores.mybatis.interfaces;

import java.util.List;
import net.infractores.entity.CustodioEntity;
import net.infractores.entity.CustodioReporteEntity;
import net.infractores.entity.DominioEntity;
import net.infractores.entity.RenovacionEntity;
import net.infractores.entity.TitularEntity;
import net.infractores.entity.TituloEntity;

public interface CustodioInterfaceDAO {
    List<TitularEntity> listadoTitulares(String tipo, String buscar, String desde, String hasta, Integer pagina, Integer regxpag);
    List<TituloEntity> listadoTitulos(Integer titularId);
    List<CustodioEntity> listadoCustodios(Integer titularId, String buscar, String desde, String hasta, Integer pagina, Integer regxpag);
    List<RenovacionEntity> listadoRenovaciones(Integer custodioId);
    List<CustodioReporteEntity> listadoCustodioReporte(String tipo, String buscar);
    
    List<DominioEntity> listadoTipoPersona();
    List<DominioEntity> listadoTipoCustodio();
    List<DominioEntity> listadoMotivosPerdida();
    
    TitularEntity obtenerTitularById(Integer titularId);
    CustodioEntity obtenerCustodioById(Integer custodioId);
    
    Integer mantenimientoTitular(TitularEntity titular);
    void mantenimientoTitularAdjunto(TitularEntity titular);
    void mantenimientoTitulo(TituloEntity titulo);
    Integer mantenimientoCustodio(CustodioEntity custodio);
    void mantenimientoCustodioAdjunto(CustodioEntity custodio);
    void mantenimientoRenovacion(RenovacionEntity renovacion);
}
