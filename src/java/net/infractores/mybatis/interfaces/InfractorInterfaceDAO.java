package net.infractores.mybatis.interfaces;

import java.util.List;
import net.infractores.entity.AdjuntoEntity;
import net.infractores.entity.DominioEntity;
import net.infractores.entity.InfractorEntity;
import net.infractores.entity.SancionEntity;
import net.infractores.entity.UbigeoEntity;

public interface InfractorInterfaceDAO {
    List<InfractorEntity> listadoInfractores(String buscar, String desde, String hasta, Integer pagina, Integer regxpag);
    InfractorEntity infractorById(Integer infractorId);
    List<UbigeoEntity> listadoUbigeos(String tipo, String departamento, String provincia);
    List<SancionEntity> listadoInfractorSanciones(Integer infractorId);
    List<AdjuntoEntity> listadoInfractorAdjuntos(Integer infractorId, Integer pagina, Integer regxpag);
    AdjuntoEntity obtenerInfractorAdjunto(Integer infractorId, Integer adjuntoId);
    List<DominioEntity> listadoInfracciones();
    List<DominioEntity> listadoCaducidad();
    List<DominioEntity> listadoObservacion();
    
    Integer crudInfractorPrincipal(InfractorEntity infractor);
    void crudInfractorSecundario(InfractorEntity infractor);
    void mantenimientoInfractor(String accion, InfractorEntity infractor);
    void mantenimientoInfractorSancion(SancionEntity sancion);
    void mantenimientoInfractorAdjunto(InfractorEntity infractor);
}
