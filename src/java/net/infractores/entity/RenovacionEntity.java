package net.infractores.entity;

import java.io.Serializable;

public class RenovacionEntity implements Serializable {
    private String accion;
    private Integer custodioId;
    private Integer secuenciaId;
    private Integer flagRenovacion;
    private String resolucion;
    private String resolucionFecha;
    private Integer resolucionVigencia;
    private String vigenciaDesde;
    private String vigenciaHasta;
    private Integer estado;
    private Integer usuarioRegistro;
    private String fechaRegistro;

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Integer getCustodioId() {
        return custodioId;
    }

    public void setCustodioId(Integer custodioId) {
        this.custodioId = custodioId;
    }

    public Integer getSecuenciaId() {
        return secuenciaId;
    }

    public void setSecuenciaId(Integer secuenciaId) {
        this.secuenciaId = secuenciaId;
    }

    public Integer getFlagRenovacion() {
        return flagRenovacion;
    }

    public void setFlagRenovacion(Integer flagRenovacion) {
        this.flagRenovacion = flagRenovacion;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public String getResolucionFecha() {
        return resolucionFecha;
    }

    public void setResolucionFecha(String resolucionFecha) {
        this.resolucionFecha = resolucionFecha;
    }

    public Integer getResolucionVigencia() {
        return resolucionVigencia;
    }

    public void setResolucionVigencia(Integer resolucionVigencia) {
        this.resolucionVigencia = resolucionVigencia;
    }

    public String getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(String vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(String vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
