package net.infractores.entity;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DominioEntity implements Serializable {
    private Integer dominioId;
    private String codigoDetalle;
    private String valorDetalle;
    private String descripcionDetalle;
    private Integer estado;
    private Integer usuarioRegistro;
    private String fechaRegistro;

    public Integer getDominioId() {
        return dominioId;
    }

    public void setDominioId(Integer dominioId) {
        this.dominioId = dominioId;
    }

    public String getCodigoDetalle() {
        return codigoDetalle;
    }

    public void setCodigoDetalle(String codigoDetalle) {
        this.codigoDetalle = codigoDetalle;
    }

    public String getValorDetalle() {
        return valorDetalle;
    }

    public void setValorDetalle(String valorDetalle) {
        this.valorDetalle = valorDetalle;
    }

    public String getDescripcionDetalle() {
        return descripcionDetalle;
    }

    public void setDescripcionDetalle(String descripcionDetalle) {
        this.descripcionDetalle = descripcionDetalle;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
