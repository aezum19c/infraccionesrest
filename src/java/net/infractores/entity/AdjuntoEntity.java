package net.infractores.entity;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AdjuntoEntity implements Serializable {
    private Integer infractorId;
    private Integer adjuntoId;
    private String nombre;
    private String descripcion;
    private String ruta;
    private Integer estado;
    private String fecha;
    private Integer usuarioRegistro;

    public Integer getInfractorId() {
        return infractorId;
    }

    public void setInfractorId(Integer infractorId) {
        this.infractorId = infractorId;
    }

    public Integer getAdjuntoId() {
        return adjuntoId;
    }

    public void setAdjuntoId(Integer adjuntoId) {
        this.adjuntoId = adjuntoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    @Override
    public String toString() {
        return "AdjuntoEntity{" + "infractorId=" + infractorId + ", adjuntoId=" + adjuntoId + ", nombre=" + nombre + ", descripcion=" + descripcion + ", ruta=" + ruta + ", estado=" + estado + ", fecha=" + fecha + ", usuarioRegistro=" + usuarioRegistro + '}';
    }
}
