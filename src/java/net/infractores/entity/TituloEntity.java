package net.infractores.entity;

import java.io.Serializable;

public class TituloEntity implements Serializable {
    private String accion;
    private Integer titularId;
    private Integer secuenciaId;
    private String nroTituloHabilitante;
    private String vigenciaInicio;
    private String vigenciaFin;
    private Integer estado;
    private Integer usuarioRegistro;
    private String fechaRegistro;

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Integer getTitularId() {
        return titularId;
    }

    public void setTitularId(Integer titularId) {
        this.titularId = titularId;
    }

    public Integer getSecuenciaId() {
        return secuenciaId;
    }

    public void setSecuenciaId(Integer secuenciaId) {
        this.secuenciaId = secuenciaId;
    }

    public String getNroTituloHabilitante() {
        return nroTituloHabilitante;
    }

    public void setNroTituloHabilitante(String nroTituloHabilitante) {
        this.nroTituloHabilitante = nroTituloHabilitante;
    }

    public String getVigenciaInicio() {
        return vigenciaInicio;
    }

    public void setVigenciaInicio(String vigenciaInicio) {
        this.vigenciaInicio = vigenciaInicio;
    }

    public String getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(String vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
}
