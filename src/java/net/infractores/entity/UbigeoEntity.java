package net.infractores.entity;

import java.io.Serializable;

public class UbigeoEntity implements Serializable {
    private Integer ubigeoId;
    private String nombre;
    private String departamento;
    private String provincia;
    private String distrito;

    public Integer getUbigeoId() {
        return ubigeoId;
    }

    public void setUbigeoId(Integer ubigeoId) {
        this.ubigeoId = ubigeoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }
}
