package net.infractores.entity;

import java.io.Serializable;

public class CustodioEntity implements Serializable {
    private String accion;
    private String origen;
    private Integer custodioId;
    private Integer titularId;
    private String nombre;
    private String apellidos;
    private String dni;
    private String cargo;
    private Integer capacitacionFlag;
    private String capacitacionFecha;
    private String capacitacionNumero;
    private String capacitacionNombre;
    private String capacitacionRuta;
    private String reconocimientoNombre;
    private String reconocimientoRuta;
    private String fechaSolicitud;
    private String actoAdministrativo;
    private String actoAdministrativoFecha;
    private Integer actoAdministrativoVigencia;
    private String vigenciaDesde;
    private String vigenciaHasta;
    private String carneCodigo;
    private String carneNombre;
    private String carneRuta;
    private Integer perdidaFlag;
    private String perdidaMotivo;
    private String perdidaMotivoDes;
    private String perdidaActoAdmNombre;
    private String perdidaActoAdmRuta;
    private String perdidaFecha;
    private Integer estado;
    private Integer usuarioRegistro;
    private String fechaRegistro;
    private Integer totalRegistros;
    private Integer totalPaginas;

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Integer getCustodioId() {
        return custodioId;
    }

    public void setCustodioId(Integer custodioId) {
        this.custodioId = custodioId;
    }

    public Integer getTitularId() {
        return titularId;
    }

    public void setTitularId(Integer titularId) {
        this.titularId = titularId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Integer getCapacitacionFlag() {
        return capacitacionFlag;
    }

    public void setCapacitacionFlag(Integer capacitacionFlag) {
        this.capacitacionFlag = capacitacionFlag;
    }

    public String getCapacitacionFecha() {
        return capacitacionFecha;
    }

    public void setCapacitacionFecha(String capacitacionFecha) {
        this.capacitacionFecha = capacitacionFecha;
    }

    public String getCapacitacionNumero() {
        return capacitacionNumero;
    }

    public void setCapacitacionNumero(String capacitacionNumero) {
        this.capacitacionNumero = capacitacionNumero;
    }

    public String getCapacitacionNombre() {
        return capacitacionNombre;
    }

    public void setCapacitacionNombre(String capacitacionNombre) {
        this.capacitacionNombre = capacitacionNombre;
    }

    public String getCapacitacionRuta() {
        return capacitacionRuta;
    }

    public void setCapacitacionRuta(String capacitacionRuta) {
        this.capacitacionRuta = capacitacionRuta;
    }

    public String getReconocimientoNombre() {
        return reconocimientoNombre;
    }

    public void setReconocimientoNombre(String reconocimientoNombre) {
        this.reconocimientoNombre = reconocimientoNombre;
    }

    public String getReconocimientoRuta() {
        return reconocimientoRuta;
    }

    public void setReconocimientoRuta(String reconocimientoRuta) {
        this.reconocimientoRuta = reconocimientoRuta;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getActoAdministrativo() {
        return actoAdministrativo;
    }

    public void setActoAdministrativo(String actoAdministrativo) {
        this.actoAdministrativo = actoAdministrativo;
    }

    public String getActoAdministrativoFecha() {
        return actoAdministrativoFecha;
    }

    public void setActoAdministrativoFecha(String actoAdministrativoFecha) {
        this.actoAdministrativoFecha = actoAdministrativoFecha;
    }

    public Integer getActoAdministrativoVigencia() {
        return actoAdministrativoVigencia;
    }

    public void setActoAdministrativoVigencia(Integer actoAdministrativoVigencia) {
        this.actoAdministrativoVigencia = actoAdministrativoVigencia;
    }

    public String getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(String vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(String vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public String getCarneCodigo() {
        return carneCodigo;
    }

    public void setCarneCodigo(String carneCodigo) {
        this.carneCodigo = carneCodigo;
    }

    public String getCarneNombre() {
        return carneNombre;
    }

    public void setCarneNombre(String carneNombre) {
        this.carneNombre = carneNombre;
    }

    public String getCarneRuta() {
        return carneRuta;
    }

    public void setCarneRuta(String carneRuta) {
        this.carneRuta = carneRuta;
    }

    public Integer getPerdidaFlag() {
        return perdidaFlag;
    }

    public void setPerdidaFlag(Integer perdidaFlag) {
        this.perdidaFlag = perdidaFlag;
    }

    public String getPerdidaMotivo() {
        return perdidaMotivo;
    }

    public void setPerdidaMotivo(String perdidaMotivo) {
        this.perdidaMotivo = perdidaMotivo;
    }

    public String getPerdidaMotivoDes() {
        return perdidaMotivoDes;
    }

    public void setPerdidaMotivoDes(String perdidaMotivoDes) {
        this.perdidaMotivoDes = perdidaMotivoDes;
    }

    public String getPerdidaActoAdmNombre() {
        return perdidaActoAdmNombre;
    }

    public void setPerdidaActoAdmNombre(String perdidaActoAdmNombre) {
        this.perdidaActoAdmNombre = perdidaActoAdmNombre;
    }

    public String getPerdidaActoAdmRuta() {
        return perdidaActoAdmRuta;
    }

    public void setPerdidaActoAdmRuta(String perdidaActoAdmRuta) {
        this.perdidaActoAdmRuta = perdidaActoAdmRuta;
    }

    public String getPerdidaFecha() {
        return perdidaFecha;
    }

    public void setPerdidaFecha(String perdidaFecha) {
        this.perdidaFecha = perdidaFecha;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Integer getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(Integer totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }
}
