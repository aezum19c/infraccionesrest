package net.infractores.entity;

import java.io.Serializable;

public class TitularEntity implements Serializable {
    private Integer titularId;
    private String accion;
    private String tipoCustodio;
    private String tipoCustodioDes;
    private String nroTituloHabilitante;
    private String vigenciaInicio;
    private String vigenciaFin;
    private String nombreTituloHabilitante;
    private String tipoPersona;
    private String tipoPersonaDes;
    private String nombreTitularComunidad;
    private String dniRucTitular;
    private String nombreRepLegal;
    private String dniRucRepLegal;
    private String ambitoTerritorial;
    private Integer ubigeoId;
    private String departamento;
    private String provincia;
    private String distrito;
    private String departamentoNombre;
    private String provinciaNombre;
    private String distritoNombre;
    private String nombreUbigeo;
    private String extension;
    private String comiteActoReconocimiento;
    private String fechaActoReconocimiento;
    private Integer vigencia;
    private String periodoDesde;
    private String periodoHasta;
    private String fechaSolicitud;
    private String nombreAdjunto;
    private String rutaAdjunto;
    private String custodioNombres;
    private String custodioCarne;
    private String custodioActoAdministrativo;
    private String custodioActoAdministrativoFecha;
    private Integer estado;
    private Integer usuarioRegistro;
    private String fechaRegistro;
    private Integer totalRegistros;
    private Integer totalPaginas;

    public Integer getTitularId() {
        return titularId;
    }

    public void setTitularId(Integer titularId) {
        this.titularId = titularId;
    }

    public String getTipoCustodio() {
        return tipoCustodio;
    }

    public void setTipoCustodio(String tipoCustodio) {
        this.tipoCustodio = tipoCustodio;
    }

    public String getTipoCustodioDes() {
        return tipoCustodioDes;
    }

    public void setTipoCustodioDes(String tipoCustodioDes) {
        this.tipoCustodioDes = tipoCustodioDes;
    }

    public String getNroTituloHabilitante() {
        return nroTituloHabilitante;
    }

    public void setNroTituloHabilitante(String nroTituloHabilitante) {
        this.nroTituloHabilitante = nroTituloHabilitante;
    }

    public String getVigenciaInicio() {
        return vigenciaInicio;
    }

    public void setVigenciaInicio(String vigenciaInicio) {
        this.vigenciaInicio = vigenciaInicio;
    }

    public String getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(String vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }

    public String getNombreTituloHabilitante() {
        return nombreTituloHabilitante;
    }

    public void setNombreTituloHabilitante(String nombreTituloHabilitante) {
        this.nombreTituloHabilitante = nombreTituloHabilitante;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoPersonaDes() {
        return tipoPersonaDes;
    }

    public void setTipoPersonaDes(String tipoPersonaDes) {
        this.tipoPersonaDes = tipoPersonaDes;
    }

    public String getNombreTitularComunidad() {
        return nombreTitularComunidad;
    }

    public void setNombreTitularComunidad(String nombreTitularComunidad) {
        this.nombreTitularComunidad = nombreTitularComunidad;
    }

    public String getDniRucTitular() {
        return dniRucTitular;
    }

    public void setDniRucTitular(String dniRucTitular) {
        this.dniRucTitular = dniRucTitular;
    }

    public String getNombreRepLegal() {
        return nombreRepLegal;
    }

    public void setNombreRepLegal(String nombreRepLegal) {
        this.nombreRepLegal = nombreRepLegal;
    }

    public String getDniRucRepLegal() {
        return dniRucRepLegal;
    }

    public void setDniRucRepLegal(String dniRucRepLegal) {
        this.dniRucRepLegal = dniRucRepLegal;
    }

    public String getAmbitoTerritorial() {
        return ambitoTerritorial;
    }

    public void setAmbitoTerritorial(String ambitoTerritorial) {
        this.ambitoTerritorial = ambitoTerritorial;
    }

    public Integer getUbigeoId() {
        return ubigeoId;
    }

    public void setUbigeoId(Integer ubigeoId) {
        this.ubigeoId = ubigeoId;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getProvinciaNombre() {
        return provinciaNombre;
    }

    public void setProvinciaNombre(String provinciaNombre) {
        this.provinciaNombre = provinciaNombre;
    }

    public String getDistritoNombre() {
        return distritoNombre;
    }

    public void setDistritoNombre(String distritoNombre) {
        this.distritoNombre = distritoNombre;
    }

    public String getNombreUbigeo() {
        return nombreUbigeo;
    }

    public void setNombreUbigeo(String nombreUbigeo) {
        this.nombreUbigeo = nombreUbigeo;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getComiteActoReconocimiento() {
        return comiteActoReconocimiento;
    }

    public void setComiteActoReconocimiento(String comiteActoReconocimiento) {
        this.comiteActoReconocimiento = comiteActoReconocimiento;
    }

    public String getFechaActoReconocimiento() {
        return fechaActoReconocimiento;
    }

    public void setFechaActoReconocimiento(String fechaActoReconocimiento) {
        this.fechaActoReconocimiento = fechaActoReconocimiento;
    }

    public Integer getVigencia() {
        return vigencia;
    }

    public void setVigencia(Integer vigencia) {
        this.vigencia = vigencia;
    }

    public String getPeriodoDesde() {
        return periodoDesde;
    }

    public void setPeriodoDesde(String periodoDesde) {
        this.periodoDesde = periodoDesde;
    }

    public String getPeriodoHasta() {
        return periodoHasta;
    }

    public void setPeriodoHasta(String periodoHasta) {
        this.periodoHasta = periodoHasta;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getNombreAdjunto() {
        return nombreAdjunto;
    }

    public void setNombreAdjunto(String nombreAdjunto) {
        this.nombreAdjunto = nombreAdjunto;
    }

    public String getRutaAdjunto() {
        return rutaAdjunto;
    }

    public void setRutaAdjunto(String rutaAdjunto) {
        this.rutaAdjunto = rutaAdjunto;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(Integer totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getCustodioNombres() {
        return custodioNombres;
    }

    public void setCustodioNombres(String custodioNombres) {
        this.custodioNombres = custodioNombres;
    }

    public String getCustodioCarne() {
        return custodioCarne;
    }

    public void setCustodioCarne(String custodioCarne) {
        this.custodioCarne = custodioCarne;
    }

    public String getCustodioActoAdministrativo() {
        return custodioActoAdministrativo;
    }

    public void setCustodioActoAdministrativo(String custodioActoAdministrativo) {
        this.custodioActoAdministrativo = custodioActoAdministrativo;
    }

    public String getCustodioActoAdministrativoFecha() {
        return custodioActoAdministrativoFecha;
    }

    public void setCustodioActoAdministrativoFecha(String custodioActoAdministrativoFecha) {
        this.custodioActoAdministrativoFecha = custodioActoAdministrativoFecha;
    }
}
