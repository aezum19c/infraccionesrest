package net.infractores.entity;

import java.io.Serializable;

public class SancionEntity implements Serializable {
    private String accion;
    private Integer infractorId;
    private Integer secuenciaId;
    private String tipoInfraccion;
    private String tipoInfraccionDes;
    private String baseLegal;
    private Double multa;
    private Integer flagMulta;
    private String medidaAdministrativa;
    private Integer cumplioMedidaAdm;
    private Integer estado;
    private Integer usuarioRegistro;
    private String fechaRegistro;

    public Integer getInfractorId() {
        return infractorId;
    }

    public void setInfractorId(Integer infractorId) {
        this.infractorId = infractorId;
    }

    public Integer getSecuenciaId() {
        return secuenciaId;
    }

    public void setSecuenciaId(Integer secuenciaId) {
        this.secuenciaId = secuenciaId;
    }

    public String getTipoInfraccion() {
        return tipoInfraccion;
    }

    public void setTipoInfraccion(String tipoInfraccion) {
        this.tipoInfraccion = tipoInfraccion;
    }

    public String getTipoInfraccionDes() {
        return tipoInfraccionDes;
    }

    public void setTipoInfraccionDes(String tipoInfraccionDes) {
        this.tipoInfraccionDes = tipoInfraccionDes;
    }

    public String getBaseLegal() {
        return baseLegal;
    }

    public void setBaseLegal(String baseLegal) {
        this.baseLegal = baseLegal;
    }

    public Double getMulta() {
        return multa;
    }

    public void setMulta(Double multa) {
        this.multa = multa;
    }

    public Integer getFlagMulta() {
        return flagMulta;
    }

    public void setFlagMulta(Integer flagMulta) {
        this.flagMulta = flagMulta;
    }

    public String getMedidaAdministrativa() {
        return medidaAdministrativa;
    }

    public void setMedidaAdministrativa(String medidaAdministrativa) {
        this.medidaAdministrativa = medidaAdministrativa;
    }

    public Integer getCumplioMedidaAdm() {
        return cumplioMedidaAdm;
    }

    public void setCumplioMedidaAdm(Integer cumplioMedidaAdm) {
        this.cumplioMedidaAdm = cumplioMedidaAdm;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }
}
