package net.infractores.entity;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InfractorEntity implements Serializable {
    private String accion;
    private Integer infractorId;
    private String nroRegistro;
    private String nombresRazonSocial;
    private String dniRuc;
    private String fecha;
    private String domicilioLegal;
    private Integer ubigeoId;
    private String primeraNroResolAdm;
    private String primeraFechaEmision;
    private String primeraFechaNotificacion;
    private Integer primeraFlagAdjunto;
    private String primeraNombreAdjunto;
    private String primeraRutaAdjunto;
    private String segundaNroResolAdm;
    private String segundaFechaEmision;
    private String segundaFechaNotificacion;
    private Integer segundaFlagAdjunto;
    private String segundaNombreAdjunto;
    private String segundaRutaAdjunto;
    private String ejecucionNroResolucion;
    private String ejecucionFechaEmision;
    private String ejecucionFechaNotificacion;
    private String coactivaNroResolucion;
    private String coactivaFechaEmision;
    private String coactivaFechaNotificacion;
    private String departamento;
    private String provincia;
    private String distrito;
    private String departamentoNombre;
    private String provinciaNombre;
    private String distritoNombre;
    private String nombreUbigeo;
    private String origen;
    private Integer estado;
    private Integer usuarioRegistro;
    private String fechaRegistro;
    private String fechaModificacion;
    private Integer totalRegistros;
    private Integer totalPaginas;

    public Integer getInfractorId() {
        return infractorId;
    }

    public void setInfractorId(Integer infractorId) {
        this.infractorId = infractorId;
    }

    public String getNroRegistro() {
        return nroRegistro;
    }

    public void setNroRegistro(String nroRegistro) {
        this.nroRegistro = nroRegistro;
    }

    public String getNombresRazonSocial() {
        return nombresRazonSocial;
    }

    public void setNombresRazonSocial(String nombresRazonSocial) {
        this.nombresRazonSocial = nombresRazonSocial;
    }

    public String getDniRuc() {
        return dniRuc;
    }

    public void setDniRuc(String dniRuc) {
        this.dniRuc = dniRuc;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDomicilioLegal() {
        return domicilioLegal;
    }

    public void setDomicilioLegal(String domicilioLegal) {
        this.domicilioLegal = domicilioLegal;
    }

    public Integer getUbigeoId() {
        return ubigeoId;
    }

    public void setUbigeoId(Integer ubigeoId) {
        this.ubigeoId = ubigeoId;
    }

    public String getPrimeraNroResolAdm() {
        return primeraNroResolAdm;
    }

    public void setPrimeraNroResolAdm(String primeraNroResolAdm) {
        this.primeraNroResolAdm = primeraNroResolAdm;
    }

    public String getPrimeraFechaEmision() {
        return primeraFechaEmision;
    }

    public void setPrimeraFechaEmision(String primeraFechaEmision) {
        this.primeraFechaEmision = primeraFechaEmision;
    }

    public String getPrimeraFechaNotificacion() {
        return primeraFechaNotificacion;
    }

    public void setPrimeraFechaNotificacion(String primeraFechaNotificacion) {
        this.primeraFechaNotificacion = primeraFechaNotificacion;
    }

    public Integer getPrimeraFlagAdjunto() {
        return primeraFlagAdjunto;
    }

    public void setPrimeraFlagAdjunto(Integer primeraFlagAdjunto) {
        this.primeraFlagAdjunto = primeraFlagAdjunto;
    }

    public String getPrimeraNombreAdjunto() {
        return primeraNombreAdjunto;
    }

    public void setPrimeraNombreAdjunto(String primeraNombreAdjunto) {
        this.primeraNombreAdjunto = primeraNombreAdjunto;
    }

    public String getPrimeraRutaAdjunto() {
        return primeraRutaAdjunto;
    }

    public void setPrimeraRutaAdjunto(String primeraRutaAdjunto) {
        this.primeraRutaAdjunto = primeraRutaAdjunto;
    }

    public String getSegundaNroResolAdm() {
        return segundaNroResolAdm;
    }

    public void setSegundaNroResolAdm(String segundaNroResolAdm) {
        this.segundaNroResolAdm = segundaNroResolAdm;
    }

    public String getSegundaFechaEmision() {
        return segundaFechaEmision;
    }

    public void setSegundaFechaEmision(String segundaFechaEmision) {
        this.segundaFechaEmision = segundaFechaEmision;
    }

    public String getSegundaFechaNotificacion() {
        return segundaFechaNotificacion;
    }

    public void setSegundaFechaNotificacion(String segundaFechaNotificacion) {
        this.segundaFechaNotificacion = segundaFechaNotificacion;
    }

    public Integer getSegundaFlagAdjunto() {
        return segundaFlagAdjunto;
    }

    public void setSegundaFlagAdjunto(Integer segundaFlagAdjunto) {
        this.segundaFlagAdjunto = segundaFlagAdjunto;
    }

    public String getSegundaNombreAdjunto() {
        return segundaNombreAdjunto;
    }

    public void setSegundaNombreAdjunto(String segundaNombreAdjunto) {
        this.segundaNombreAdjunto = segundaNombreAdjunto;
    }

    public String getSegundaRutaAdjunto() {
        return segundaRutaAdjunto;
    }

    public void setSegundaRutaAdjunto(String segundaRutaAdjunto) {
        this.segundaRutaAdjunto = segundaRutaAdjunto;
    }

    public String getEjecucionNroResolucion() {
        return ejecucionNroResolucion;
    }

    public void setEjecucionNroResolucion(String ejecucionNroResolucion) {
        this.ejecucionNroResolucion = ejecucionNroResolucion;
    }

    public String getEjecucionFechaEmision() {
        return ejecucionFechaEmision;
    }

    public void setEjecucionFechaEmision(String ejecucionFechaEmision) {
        this.ejecucionFechaEmision = ejecucionFechaEmision;
    }

    public String getEjecucionFechaNotificacion() {
        return ejecucionFechaNotificacion;
    }

    public void setEjecucionFechaNotificacion(String ejecucionFechaNotificacion) {
        this.ejecucionFechaNotificacion = ejecucionFechaNotificacion;
    }

    public String getCoactivaNroResolucion() {
        return coactivaNroResolucion;
    }

    public void setCoactivaNroResolucion(String coactivaNroResolucion) {
        this.coactivaNroResolucion = coactivaNroResolucion;
    }

    public String getCoactivaFechaEmision() {
        return coactivaFechaEmision;
    }

    public void setCoactivaFechaEmision(String coactivaFechaEmision) {
        this.coactivaFechaEmision = coactivaFechaEmision;
    }

    public String getCoactivaFechaNotificacion() {
        return coactivaFechaNotificacion;
    }

    public void setCoactivaFechaNotificacion(String coactivaFechaNotificacion) {
        this.coactivaFechaNotificacion = coactivaFechaNotificacion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getNombreUbigeo() {
        return nombreUbigeo;
    }

    public void setNombreUbigeo(String nombreUbigeo) {
        this.nombreUbigeo = nombreUbigeo;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getTotalRegistros() {
        return totalRegistros;
    }

    public void setTotalRegistros(Integer totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public Integer getTotalPaginas() {
        return totalPaginas;
    }

    public void setTotalPaginas(Integer totalPaginas) {
        this.totalPaginas = totalPaginas;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getDepartamentoNombre() {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre) {
        this.departamentoNombre = departamentoNombre;
    }

    public String getProvinciaNombre() {
        return provinciaNombre;
    }

    public void setProvinciaNombre(String provinciaNombre) {
        this.provinciaNombre = provinciaNombre;
    }

    public String getDistritoNombre() {
        return distritoNombre;
    }

    public void setDistritoNombre(String distritoNombre) {
        this.distritoNombre = distritoNombre;
    }

    @Override
    public String toString() {
        return "InfractorEntity{" + "accion=" + accion + ", infractorId=" + infractorId + ", nroRegistro=" + nroRegistro + ", nombresRazonSocial=" + nombresRazonSocial + ", dniRuc=" + dniRuc + ", fecha=" + fecha + ", domicilioLegal=" + domicilioLegal + ", ubigeoId=" + ubigeoId + ", primeraNroResolAdm=" + primeraNroResolAdm + ", primeraFechaEmision=" + primeraFechaEmision + ", primeraFechaNotificacion=" + primeraFechaNotificacion + ", primeraFlagAdjunto=" + primeraFlagAdjunto + ", primeraNombreAdjunto=" + primeraNombreAdjunto + ", primeraRutaAdjunto=" + primeraRutaAdjunto + ", segundaNroResolAdm=" + segundaNroResolAdm + ", segundaFechaEmision=" + segundaFechaEmision + ", segundaFechaNotificacion=" + segundaFechaNotificacion + ", segundaFlagAdjunto=" + segundaFlagAdjunto + ", segundaNombreAdjunto=" + segundaNombreAdjunto + ", segundaRutaAdjunto=" + segundaRutaAdjunto + ", ejecucionNroResolucion=" + ejecucionNroResolucion + ", ejecucionFechaEmision=" + ejecucionFechaEmision + ", ejecucionFechaNotificacion=" + ejecucionFechaNotificacion + ", coactivaNroResolucion=" + coactivaNroResolucion + ", coactivaFechaEmision=" + coactivaFechaEmision + ", coactivaFechaNotificacion=" + coactivaFechaNotificacion + ", departamento=" + departamento + ", provincia=" + provincia + ", distrito=" + distrito + ", departamentoNombre=" + departamentoNombre + ", provinciaNombre=" + provinciaNombre + ", distritoNombre=" + distritoNombre + ", nombreUbigeo=" + nombreUbigeo + ", estado=" + estado + ", usuarioRegistro=" + usuarioRegistro + ", fechaRegistro=" + fechaRegistro + ", totalRegistros=" + totalRegistros + ", totalPaginas=" + totalPaginas + '}';
    }

    public String getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(String fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
}
