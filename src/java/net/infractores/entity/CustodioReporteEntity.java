package net.infractores.entity;

import java.io.Serializable;

public class CustodioReporteEntity implements Serializable {
    private Integer titularId;
    private Integer custodioId;
    private Integer renovacionId;
    private String nroTituloHabilitante;
    private String nombreTituloHabilitante;
    private String tipoPersona;
    private String tipoPersonaDes;
    private String nombreTitularComunidad;
    private String dniRucTitular;
    private String vigenciaInicio;
    private String vigenciaFin;
    private String nombreRepLegal;
    private String dniRucRepLegal;
    private String ambitoTerritorial;
    private String extension;
    private String comiteActoReconocimiento;
    private Integer vigencia;
    private String fechaActoReconocimiento;
    private String periodoDesde;
    private String periodoHasta;
    private String fechaSolicitud;
    private String distritoNombre;
    private Integer capacitacionFlag;
    private String capacitacionFecha;
    private String provinciaNombre;
    private Integer tituloSecuenciaId;
    private String tituloNroTituloHabilitante;
    private String tituloVigenciaInicio;
    private String tituloVigenciaFin;
    private String nombre;
    private String apellidos;
    private String cargo;
    private String dni;
    private String capacitacionNumero;
    private String actoAdministrativo;
    private String custodioFechaSolicitud;
    private String actoAdministrativoFecha;
    private Integer actoAdministrativoVigencia;
    private String vigenciaDesde;
    private String vigenciaHasta;
    private String carneCodigo;
    private Integer perdidaFlag;
    private String perdidaMotivoDes;
    private String perdidaFecha;
    private Integer renovacionFlag;
    private String renovacionResolucion;
    private String renovacionResolucionFecha;
    private Integer renovacionResolucionVigencia;
    private String renovacionVigenciaDesde;
    private String renovacionVigenciaHasta;
    private Integer flagAdjuntoTitular;
    private Integer flagAdjuntoCapacitacion;
    private Integer flagAdjuntoReconocimiento;
    private Integer flagAdjuntoCarne;
    private Integer flagAdjuntoPerdidaActoAdm;

    public String getNroTituloHabilitante() {
        return nroTituloHabilitante;
    }

    public void setNroTituloHabilitante(String nroTituloHabilitante) {
        this.nroTituloHabilitante = nroTituloHabilitante;
    }

    public String getNombreTituloHabilitante() {
        return nombreTituloHabilitante;
    }

    public void setNombreTituloHabilitante(String nombreTituloHabilitante) {
        this.nombreTituloHabilitante = nombreTituloHabilitante;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoPersonaDes() {
        return tipoPersonaDes;
    }

    public void setTipoPersonaDes(String tipoPersonaDes) {
        this.tipoPersonaDes = tipoPersonaDes;
    }

    public String getNombreTitularComunidad() {
        return nombreTitularComunidad;
    }

    public void setNombreTitularComunidad(String nombreTitularComunidad) {
        this.nombreTitularComunidad = nombreTitularComunidad;
    }

    public String getDniRucTitular() {
        return dniRucTitular;
    }

    public void setDniRucTitular(String dniRucTitular) {
        this.dniRucTitular = dniRucTitular;
    }

    public String getVigenciaInicio() {
        return vigenciaInicio;
    }

    public void setVigenciaInicio(String vigenciaInicio) {
        this.vigenciaInicio = vigenciaInicio;
    }

    public String getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(String vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }

    public String getNombreRepLegal() {
        return nombreRepLegal;
    }

    public void setNombreRepLegal(String nombreRepLegal) {
        this.nombreRepLegal = nombreRepLegal;
    }

    public String getDniRucRepLegal() {
        return dniRucRepLegal;
    }

    public void setDniRucRepLegal(String dniRucRepLegal) {
        this.dniRucRepLegal = dniRucRepLegal;
    }

    public String getAmbitoTerritorial() {
        return ambitoTerritorial;
    }

    public void setAmbitoTerritorial(String ambitoTerritorial) {
        this.ambitoTerritorial = ambitoTerritorial;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getComiteActoReconocimiento() {
        return comiteActoReconocimiento;
    }

    public void setComiteActoReconocimiento(String comiteActoReconocimiento) {
        this.comiteActoReconocimiento = comiteActoReconocimiento;
    }

    public Integer getVigencia() {
        return vigencia;
    }

    public void setVigencia(Integer vigencia) {
        this.vigencia = vigencia;
    }

    public String getFechaActoReconocimiento() {
        return fechaActoReconocimiento;
    }

    public void setFechaActoReconocimiento(String fechaActoReconocimiento) {
        this.fechaActoReconocimiento = fechaActoReconocimiento;
    }

    public String getPeriodoDesde() {
        return periodoDesde;
    }

    public void setPeriodoDesde(String periodoDesde) {
        this.periodoDesde = periodoDesde;
    }

    public String getPeriodoHasta() {
        return periodoHasta;
    }

    public void setPeriodoHasta(String periodoHasta) {
        this.periodoHasta = periodoHasta;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getDistritoNombre() {
        return distritoNombre;
    }

    public void setDistritoNombre(String distritoNombre) {
        this.distritoNombre = distritoNombre;
    }

    public Integer getCapacitacionFlag() {
        return capacitacionFlag;
    }

    public void setCapacitacionFlag(Integer capacitacionFlag) {
        this.capacitacionFlag = capacitacionFlag;
    }

    public String getCapacitacionFecha() {
        return capacitacionFecha;
    }

    public void setCapacitacionFecha(String capacitacionFecha) {
        this.capacitacionFecha = capacitacionFecha;
    }

    public String getProvinciaNombre() {
        return provinciaNombre;
    }

    public void setProvinciaNombre(String provinciaNombre) {
        this.provinciaNombre = provinciaNombre;
    }

    public Integer getTituloSecuenciaId() {
        return tituloSecuenciaId;
    }

    public void setTituloSecuenciaId(Integer tituloSecuenciaId) {
        this.tituloSecuenciaId = tituloSecuenciaId;
    }

    public String getTituloNroTituloHabilitante() {
        return tituloNroTituloHabilitante;
    }

    public void setTituloNroTituloHabilitante(String tituloNroTituloHabilitante) {
        this.tituloNroTituloHabilitante = tituloNroTituloHabilitante;
    }

    public String getTituloVigenciaInicio() {
        return tituloVigenciaInicio;
    }

    public void setTituloVigenciaInicio(String tituloVigenciaInicio) {
        this.tituloVigenciaInicio = tituloVigenciaInicio;
    }

    public String getTituloVigenciaFin() {
        return tituloVigenciaFin;
    }

    public void setTituloVigenciaFin(String tituloVigenciaFin) {
        this.tituloVigenciaFin = tituloVigenciaFin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCapacitacionNumero() {
        return capacitacionNumero;
    }

    public void setCapacitacionNumero(String capacitacionNumero) {
        this.capacitacionNumero = capacitacionNumero;
    }

    public String getActoAdministrativo() {
        return actoAdministrativo;
    }

    public void setActoAdministrativo(String actoAdministrativo) {
        this.actoAdministrativo = actoAdministrativo;
    }

    public String getCustodioFechaSolicitud() {
        return custodioFechaSolicitud;
    }

    public void setCustodioFechaSolicitud(String custodioFechaSolicitud) {
        this.custodioFechaSolicitud = custodioFechaSolicitud;
    }

    public String getActoAdministrativoFecha() {
        return actoAdministrativoFecha;
    }

    public void setActoAdministrativoFecha(String actoAdministrativoFecha) {
        this.actoAdministrativoFecha = actoAdministrativoFecha;
    }

    public Integer getActoAdministrativoVigencia() {
        return actoAdministrativoVigencia;
    }

    public void setActoAdministrativoVigencia(Integer actoAdministrativoVigencia) {
        this.actoAdministrativoVigencia = actoAdministrativoVigencia;
    }

    public String getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(String vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public String getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(String vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public String getCarneCodigo() {
        return carneCodigo;
    }

    public void setCarneCodigo(String carneCodigo) {
        this.carneCodigo = carneCodigo;
    }

    public Integer getPerdidaFlag() {
        return perdidaFlag;
    }

    public void setPerdidaFlag(Integer perdidaFlag) {
        this.perdidaFlag = perdidaFlag;
    }

    public String getPerdidaMotivoDes() {
        return perdidaMotivoDes;
    }

    public void setPerdidaMotivoDes(String perdidaMotivoDes) {
        this.perdidaMotivoDes = perdidaMotivoDes;
    }

    public String getPerdidaFecha() {
        return perdidaFecha;
    }

    public void setPerdidaFecha(String perdidaFecha) {
        this.perdidaFecha = perdidaFecha;
    }

    public Integer getRenovacionFlag() {
        return renovacionFlag;
    }

    public void setRenovacionFlag(Integer renovacionFlag) {
        this.renovacionFlag = renovacionFlag;
    }

    public String getRenovacionResolucion() {
        return renovacionResolucion;
    }

    public void setRenovacionResolucion(String renovacionResolucion) {
        this.renovacionResolucion = renovacionResolucion;
    }

    public String getRenovacionResolucionFecha() {
        return renovacionResolucionFecha;
    }

    public void setRenovacionResolucionFecha(String renovacionResolucionFecha) {
        this.renovacionResolucionFecha = renovacionResolucionFecha;
    }

    public Integer getRenovacionResolucionVigencia() {
        return renovacionResolucionVigencia;
    }

    public void setRenovacionResolucionVigencia(Integer renovacionResolucionVigencia) {
        this.renovacionResolucionVigencia = renovacionResolucionVigencia;
    }

    public String getRenovacionVigenciaDesde() {
        return renovacionVigenciaDesde;
    }

    public void setRenovacionVigenciaDesde(String renovacionVigenciaDesde) {
        this.renovacionVigenciaDesde = renovacionVigenciaDesde;
    }

    public String getRenovacionVigenciaHasta() {
        return renovacionVigenciaHasta;
    }

    public void setRenovacionVigenciaHasta(String renovacionVigenciaHasta) {
        this.renovacionVigenciaHasta = renovacionVigenciaHasta;
    }

    public Integer getFlagAdjuntoTitular() {
        return flagAdjuntoTitular;
    }

    public void setFlagAdjuntoTitular(Integer flagAdjuntoTitular) {
        this.flagAdjuntoTitular = flagAdjuntoTitular;
    }

    public Integer getFlagAdjuntoCapacitacion() {
        return flagAdjuntoCapacitacion;
    }

    public void setFlagAdjuntoCapacitacion(Integer flagAdjuntoCapacitacion) {
        this.flagAdjuntoCapacitacion = flagAdjuntoCapacitacion;
    }

    public Integer getFlagAdjuntoReconocimiento() {
        return flagAdjuntoReconocimiento;
    }

    public void setFlagAdjuntoReconocimiento(Integer flagAdjuntoReconocimiento) {
        this.flagAdjuntoReconocimiento = flagAdjuntoReconocimiento;
    }

    public Integer getFlagAdjuntoCarne() {
        return flagAdjuntoCarne;
    }

    public void setFlagAdjuntoCarne(Integer flagAdjuntoCarne) {
        this.flagAdjuntoCarne = flagAdjuntoCarne;
    }

    public Integer getFlagAdjuntoPerdidaActoAdm() {
        return flagAdjuntoPerdidaActoAdm;
    }

    public void setFlagAdjuntoPerdidaActoAdm(Integer flagAdjuntoPerdidaActoAdm) {
        this.flagAdjuntoPerdidaActoAdm = flagAdjuntoPerdidaActoAdm;
    }

    public Integer getTitularId() {
        return titularId;
    }

    public void setTitularId(Integer titularId) {
        this.titularId = titularId;
    }

    public Integer getCustodioId() {
        return custodioId;
    }

    public void setCustodioId(Integer custodioId) {
        this.custodioId = custodioId;
    }

    public Integer getRenovacionId() {
        return renovacionId;
    }

    public void setRenovacionId(Integer renovacionId) {
        this.renovacionId = renovacionId;
    }
}
