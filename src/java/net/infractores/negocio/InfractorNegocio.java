package net.infractores.negocio;

import java.util.ArrayList;
import java.util.List;
import net.infractores.entity.AdjuntoEntity;
import net.infractores.entity.DominioEntity;
import net.infractores.entity.InfractorEntity;
import net.infractores.entity.SancionEntity;
import net.infractores.entity.UbigeoEntity;
import net.infractores.mybatis.dao.InfractorDAOSQL;
import net.infractores.mybatis.interfaces.InfractorInterfaceNegocio;

@SuppressWarnings({ "FieldMayBeFinal", "Convert2Diamond", "UnusedAssignment"})
public class InfractorNegocio implements InfractorInterfaceNegocio {
    private InfractorDAOSQL infractorDAOSQL;

    public InfractorNegocio(InfractorDAOSQL infractorDAOSQL) {
        this.infractorDAOSQL = infractorDAOSQL;
    }

    @Override
    public List<InfractorEntity> listadoInfractores(String buscar, String desde, String hasta, Integer pagina, Integer regxpag) {
        List<InfractorEntity> infractores = new ArrayList<InfractorEntity>();
        
        infractores = infractorDAOSQL.listadoInfractores(buscar, desde, hasta, pagina, regxpag);
        
        return infractores;
    }

    @Override
    public InfractorEntity infractorById(Integer infractorId) {
        InfractorEntity adjunto = new InfractorEntity();
        
        adjunto = infractorDAOSQL.infractorById(infractorId);
        
        return adjunto;
    }
    
    @Override
    public List<UbigeoEntity> listadoUbigeos(String tipo, String departamento, String provincia) {
        List<UbigeoEntity> ubigeos = new ArrayList<UbigeoEntity>();
        
        ubigeos = infractorDAOSQL.listadoUbigeos(tipo, departamento, provincia);
        
        return ubigeos;
    }
    
    @Override
    public List<SancionEntity> listadoInfractorSanciones(Integer infractorId) {
        List<SancionEntity> sanciones = new ArrayList<SancionEntity>();
        
        sanciones = infractorDAOSQL.listadoInfractorSanciones(infractorId);
        
        return sanciones;
    }

    @Override
    public List<AdjuntoEntity> listadoInfractorAdjuntos(Integer infractorId, Integer pagina, Integer regxpag) {
        List<AdjuntoEntity> adjuntos = new ArrayList<AdjuntoEntity>();
        
        adjuntos = infractorDAOSQL.listadoInfractorAdjuntos(infractorId, pagina, regxpag);
        
        return adjuntos;
    }

    @Override
    public AdjuntoEntity obtenerInfractorAdjunto(Integer infractorId, Integer adjuntoId) {
        AdjuntoEntity adjunto = new AdjuntoEntity();
        
        adjunto = infractorDAOSQL.obtenerInfractorAdjunto(infractorId, adjuntoId);
        
        return adjunto;
    }

    @Override
    public List<DominioEntity> listadoInfracciones() {
        List<DominioEntity> caducidades = new ArrayList<DominioEntity>();
        
        caducidades = infractorDAOSQL.listadoInfracciones();
        
        return caducidades;
    }

    @Override
    public List<DominioEntity> listadoCaducidad() {
        List<DominioEntity> caducidades = new ArrayList<DominioEntity>();
        
        caducidades = infractorDAOSQL.listadoCaducidad();
        
        return caducidades;
    }

    @Override
    public List<DominioEntity> listadoObservacion() {
        List<DominioEntity> observaciones = new ArrayList<DominioEntity>();
        
        observaciones = infractorDAOSQL.listadoObservacion();
        
        return observaciones;
    }
    
    @Override
    public Integer crudInfractorPrincipal(InfractorEntity infractor) {
        return infractorDAOSQL.crudInfractorPrincipal(infractor);
    }
    
    @Override
    public void crudInfractorSecundario(InfractorEntity infractor) {
        infractorDAOSQL.crudInfractorSecundario(infractor);
    }
    
    @Override
    public void mantenimientoInfractor(String accion, InfractorEntity infractor) {
        infractorDAOSQL.mantenimientoInfractor(accion, infractor);
    }
    
    @Override
    public void mantenimientoInfractorSancion(SancionEntity sancion) {
        infractorDAOSQL.mantenimientoInfractorSancion(sancion);
    }
    
    @Override
    public void mantenimientoInfractorAdjunto(InfractorEntity infracto) {
        infractorDAOSQL.mantenimientoInfractorAdjunto(infracto);
    }
}
