package net.infractores.negocio;

import java.util.ArrayList;
import java.util.List;
import net.infractores.entity.CustodioEntity;
import net.infractores.entity.CustodioReporteEntity;
import net.infractores.entity.DominioEntity;
import net.infractores.entity.RenovacionEntity;
import net.infractores.entity.TitularEntity;
import net.infractores.entity.TituloEntity;
import net.infractores.mybatis.dao.CustodioDAOSQL;
import net.infractores.mybatis.interfaces.CustodioInterfaceNegocio;

@SuppressWarnings({ "FieldMayBeFinal", "Convert2Diamond", "UnusedAssignment"})
public class CustodioNegocio implements CustodioInterfaceNegocio {
    private CustodioDAOSQL custodioDAOSQL;
    
    public CustodioNegocio(CustodioDAOSQL custodioDAOSQL) {
        this.custodioDAOSQL = custodioDAOSQL;
    }

    @Override
    public List<TitularEntity> listadoTitulares(String tipo, String buscar, String desde, String hasta, Integer pagina, Integer regxpag){
        List<TitularEntity> titulares = new ArrayList<TitularEntity>();
        
        titulares = custodioDAOSQL.listadoTitulares(tipo, buscar, desde, hasta, pagina, regxpag);
        
        return titulares;
    }

    @Override
    public List<TituloEntity> listadoTitulos(Integer titularId){
        List<TituloEntity> titulos = new ArrayList<TituloEntity>();
        
        titulos = custodioDAOSQL.listadoTitulos(titularId);
        
        return titulos;
    }

    @Override
    public List<CustodioEntity> listadoCustodios(Integer titularId, String buscar, String desde, String hasta, Integer pagina, Integer regxpag) {
        List<CustodioEntity> custodios = new ArrayList<CustodioEntity>();
        
        custodios = custodioDAOSQL.listadoCustodios(titularId, buscar, desde, hasta, pagina, regxpag);
        
        return custodios;
    }

    @Override
    public List<RenovacionEntity> listadoRenovaciones(Integer custodioId){
        List<RenovacionEntity> renovaciones = new ArrayList<RenovacionEntity>();
        
        renovaciones = custodioDAOSQL.listadoRenovaciones(custodioId);
        
        return renovaciones;
    }

    @Override
    public List<CustodioReporteEntity> listadoCustodioReporte(String tipo, String buscar) {
        List<CustodioReporteEntity> reporte = new ArrayList<CustodioReporteEntity>();
        
        reporte = custodioDAOSQL.listadoCustodioReporte(tipo, buscar);
        
        return reporte;
    }


    @Override
    public List<DominioEntity> listadoTipoPersona() {
        List<DominioEntity> tiposPersona = new ArrayList<DominioEntity>();
        
        tiposPersona = custodioDAOSQL.listadoTipoPersona();
        
        return tiposPersona;
    }

    @Override
    public List<DominioEntity> listadoTipoCustodio() {
        List<DominioEntity> tiposCustodio = new ArrayList<DominioEntity>();
        
        tiposCustodio = custodioDAOSQL.listadoTipoCustodio();
        
        return tiposCustodio;
    }

    @Override
    public List<DominioEntity> listadoMotivosPerdida() {
        List<DominioEntity> motivoPerdida = new ArrayList<DominioEntity>();
        
        motivoPerdida = custodioDAOSQL.listadoMotivosPerdida();
        
        return motivoPerdida;
    }
    
    @Override
    public TitularEntity obtenerTitularById(Integer titularId) {
        return custodioDAOSQL.obtenerTitularById(titularId);
    }
    
    @Override
    public CustodioEntity obtenerCustodioById(Integer custodioId) {
        return custodioDAOSQL.obtenerCustodioById(custodioId);
    }
    
    @Override
    public Integer mantenimientoTitular(TitularEntity titular) {
        return custodioDAOSQL.mantenimientoTitular(titular);
    }
    
    @Override
    public void mantenimientoTitularAdjunto(TitularEntity titular) {
        custodioDAOSQL.mantenimientoTitularAdjunto(titular);
    }
    
    @Override
    public void mantenimientoTitulo(TituloEntity titulo) {
        custodioDAOSQL.mantenimientoTitulo(titulo);
    }
    
    @Override
    public Integer mantenimientoCustodio(CustodioEntity custodio) {
        return custodioDAOSQL.mantenimientoCustodio(custodio);
    }
    
    @Override
    public void mantenimientoCustodioAdjunto(CustodioEntity custodio) {
        custodioDAOSQL.mantenimientoCustodioAdjunto(custodio);
    }
    
    @Override
    public void mantenimientoRenovacion(RenovacionEntity renovacion) {
        custodioDAOSQL.mantenimientoRenovacion(renovacion);
    }
}
