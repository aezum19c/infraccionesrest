package net.infractores.negocio;

import java.util.ArrayList;
import java.util.List;
import net.infractores.entity.RolEntity;
import net.infractores.entity.UsuarioEntity;
import net.infractores.mybatis.dao.UsuarioDAOSQL;
import net.infractores.mybatis.interfaces.UsuarioInterfaceNegocio;
import net.infractores.utils.GeneralUtil;

@SuppressWarnings({ "FieldMayBeFinal", "Convert2Diamond", "UnusedAssignment"  })
public class UsuarioNegocio implements UsuarioInterfaceNegocio {
    private UsuarioDAOSQL usuarioDAOSQL;

    public UsuarioNegocio(UsuarioDAOSQL usuarioDAOSQL) {
        this.usuarioDAOSQL = usuarioDAOSQL;
    }

    @Override
    public UsuarioEntity validarUsuario(String usuario, String clave) {
        UsuarioEntity datosUsuario = null;
        
        if(!GeneralUtil.cadenaEsVacioNulo(usuario) && !GeneralUtil.cadenaEsVacioNulo(clave)){
            datosUsuario = usuarioDAOSQL.validarUsuario(usuario, GeneralUtil.encriptaEnMD5(clave));
        }
        
        return datosUsuario;
    }

    @Override
    public List<RolEntity> rolesUsuario(Integer usuarioId){
        List<RolEntity> roles = new ArrayList<RolEntity>();
        
        roles = usuarioDAOSQL.rolesUsuario(usuarioId);
        
        return roles;
    }
    
}
