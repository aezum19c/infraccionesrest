package net.infractores.response;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import net.infractores.entity.AdjuntoEntity;

@XmlRootElement
public class AdjuntoListadoResponseEntity implements Serializable {
    private Integer result_code;
    private String error_description;
    private String response_date;
    private List<AdjuntoEntity> content;

    public Integer getResult_code() {
        return result_code;
    }

    public void setResult_code(Integer result_code) {
        this.result_code = result_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public String getResponse_date() {
        return response_date;
    }

    public void setResponse_date(String response_date) {
        this.response_date = response_date;
    }

    public List<AdjuntoEntity> getContent() {
        return content;
    }

    public void setContent(List<AdjuntoEntity> content) {
        this.content = content;
    }
}
