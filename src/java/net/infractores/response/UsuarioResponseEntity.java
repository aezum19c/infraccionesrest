package net.infractores.response;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import net.infractores.entity.UsuarioEntity;

@XmlRootElement
public class UsuarioResponseEntity implements Serializable {
    private Integer result_code;
    private String error_description;
    private String response_date;
    private String accessToken;
    private UsuarioEntity usuario;

    public Integer getResult_code() {
        return result_code;
    }

    public void setResult_code(Integer result_code) {
        this.result_code = result_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public String getResponse_date() {
        return response_date;
    }

    public void setResponse_date(String response_date) {
        this.response_date = response_date;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public UsuarioEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioEntity usuario) {
        this.usuario = usuario;
    }
}
