package net.infractores.utils;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings({"ConvertToTryWithResources", "UnusedAssignment", "CallToPrintStackTrace"})
public class Log {
    public static void writeLog(String rutaLog, String nameLog, String fullMethodClass, String content) {
        try {
            String mensaje = "";
            
            String fullNameLog = rutaLog + nameLog + ".log";
            //String fullNameLog = rutaLog + nameLog + "_" + formatoFecha("yyyyMMdd") + ".log";
            
            String fecha = formatoFecha("dd/MM/yyyy HH:mm:ss");
            mensaje = fullMethodClass + ":: " + fecha + ":: " + content;
            
            System.out.println(mensaje);
            
            PrintWriter pw = new PrintWriter(new FileWriter(fullNameLog,true));
            pw.println(mensaje);
            pw.close();
        } catch(Exception exc) {
            
        }
    }
    
    public static String formatoFecha(String formato){
        String fecha_final = "";

        try {
            Date fecha = new Date();
            DateFormat df = null;

            df = new SimpleDateFormat(formato);
            fecha_final = df.format(fecha);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return fecha_final;
    }
    
    public static void showSystemOut(String fullMethodClass, String content) {
        try {
            String mensaje = "";
            
            String fecha = formatoFecha("dd/MM/yyyy HH:mm:ss");
            mensaje = fullMethodClass + ":: " + fecha + ":: " + content;
            
            System.out.println(mensaje);
        } catch(Exception exc) {
            
        }
    }
}