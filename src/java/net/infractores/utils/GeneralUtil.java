
package net.infractores.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class GeneralUtil {
    public static String getLiteralResoruce(FacesContext context, String literalResource, String literalChild){
        String literal = "";
        Application app = context.getApplication();
        ResourceBundle resource = app.getResourceBundle(context, literalResource);
        try{
            literal = resource.getString(literalChild);
        }catch(Exception ex){
            literal = "";
        }
        return literal;
    }
    
    public static void mostrarMensajePorNombres(FacesContext context, Severity severity, String literalName, String msgTitleName, String... msgContentNames){
        
        String msgContent = "";
        for(int i=0; i<msgContentNames.length; i++){
            msgContent += GeneralUtil.getLiteralResoruce(context, literalName, msgContentNames[i])+" > ";
        }
        
        context.addMessage(null, new FacesMessage(severity, GeneralUtil.getLiteralResoruce(context, literalName, msgTitleName),  msgContent.substring(0, msgContent.length()-2)) );
    }
    
    public static FacesMessage getFacesMessage(FacesContext context, Severity severity, String literalName, String msgTitleName, String msgContentName){
        return new FacesMessage(severity, GeneralUtil.getLiteralResoruce(context, literalName, msgTitleName), GeneralUtil.getLiteralResoruce(context, literalName, msgContentName));
    }

    public static Long getDateStringToInteger(String strDate){
        //strDate tiene el formato yyyy-MM-ddTHH:mm:ss.SSS
        strDate = strDate.replaceAll(".000", "").replaceAll("-", "").replaceAll("T", "").replaceAll(":", "");
        Long retorno;
        try {
            retorno = Long.parseLong(strDate);
        } catch(Exception e) {
            retorno = 0l;
        }
        
        return retorno;
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    public static int obtenerPromedioCredito(Integer eficiencia){
        int retorno = 0;
        
        if(eficiencia.equals(60)) retorno = 142;
        if(eficiencia.equals(61)) retorno = 137;
        if(eficiencia.equals(62)) retorno = 132;
        if(eficiencia.equals(63)) retorno = 127;
        if(eficiencia.equals(64)) retorno = 122;
        if(eficiencia.equals(65)) retorno = 117;
        if(eficiencia.equals(66)) retorno = 112;
        if(eficiencia.equals(67)) retorno = 107;
        if(eficiencia.equals(68)) retorno = 102;
        if(eficiencia.equals(69)) retorno = 87;
        if(eficiencia.equals(70)) retorno = 82;
        if(eficiencia.equals(71)) retorno = 79;
        if(eficiencia.equals(72)) retorno = 79;
        if(eficiencia.equals(73)) retorno = 69;
        if(eficiencia.equals(74)) retorno = 76;
        if(eficiencia.equals(75)) retorno = 61;
        if(eficiencia.equals(76)) retorno = 55;
        if(eficiencia.equals(77)) retorno = 51;
        if(eficiencia.equals(78)) retorno = 51;
        if(eficiencia.equals(79)) retorno = 41;
        if(eficiencia.equals(80)) retorno = 36;
        if(eficiencia.equals(81)) retorno = 37;
        if(eficiencia.equals(82)) retorno = 33;
        if(eficiencia.equals(83)) retorno = 32;
        if(eficiencia.equals(84)) retorno = 27;
        if(eficiencia.equals(85)) retorno = 22;
        if(eficiencia.equals(86)) retorno = 19;
        if(eficiencia.equals(87)) retorno = 16;
        if(eficiencia.equals(88)) retorno = 15;
        if(eficiencia.equals(89)) retorno = 14;
        if(eficiencia.equals(90)) retorno = 13;
        if(eficiencia.equals(91)) retorno = 11;
        if(eficiencia.equals(92)) retorno = 10;
        if(eficiencia.equals(93)) retorno = 10;
        if(eficiencia.equals(94)) retorno = 9;
        if(eficiencia.equals(95)) retorno = 8;
        if(eficiencia.equals(96)) retorno = 7;
        if(eficiencia.equals(97)) retorno = 8;
        if(eficiencia.equals(98)) retorno = 6;
        if(eficiencia.equals(99)) retorno = 6;
        
        return retorno;
    }
    
    public static String encriptaEnMD5(String stringAEncriptar){
        /*char[] CONSTS_HEX = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };
        try{
          MessageDigest msgd = MessageDigest.getInstance("MD5");
          byte[] bytes = msgd.digest(stringAEncriptar.getBytes());
          StringBuilder strbCadenaMD5 = new StringBuilder(2 * bytes.length);
          for (int i = 0; i < bytes.length; i++)
          {
              int bajo = (int)(bytes[i] & 0x0f);
              int alto = (int)((bytes[i] & 0xf0) >> 4);
              strbCadenaMD5.append(CONSTS_HEX[alto]);
              strbCadenaMD5.append(CONSTS_HEX[bajo]);
          }
          return strbCadenaMD5.toString();
       } catch (NoSuchAlgorithmException e) {
          return null;
       }*/
        
       return stringAEncriptar;
    }
    
    public static int[] obtenerIndicesAleatorios(int size){
        int[] idx = new int[size];
        
        for(int i=0; i < size; i ++){
            idx[i] = i;
        }
        
        Random rnd = ThreadLocalRandom.current();
        for (int i = idx.length - 1; i > 0; i--)
        {
          int index = rnd.nextInt(i + 1);
          int a = idx[index];
          idx[index] = idx[i];
          idx[i] = a;
        }
        
        return idx;
    }
    
    public static boolean cadenaEsVacioNulo(String cadena){
        boolean vacio = false;
        if(cadena==null){
            vacio = true;
        }else{
            if(cadena.isEmpty()){
                vacio = true;
            }
        }
        return vacio;
    }
    
    public static boolean listaEsVacioNulo(List lista){
        boolean vacio = false;
        if(lista==null){
            vacio = true;
        }else{
            if(lista.isEmpty()){
                vacio = true;
            }
        }
        return vacio;
    }
    
    public static boolean arrayEsVacioNulo(Object[] lista){
        boolean vacio = false;
        if(lista==null){
            vacio = true;
        }else{
            if(lista.length == 0){
                vacio = true;
            }
        }
        return vacio;
    }
    
    public static String limpiarCadena(String cadena){
        String cadenaFinal="";
        if(!cadenaEsVacioNulo(cadena)){
            cadenaFinal = cadena.trim();
        }
        return cadenaFinal;
    }
}
