package net.infractores.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

@SuppressWarnings({"CallToPrintStackTrace"})
public class FechaUtils {
    public static Integer getCurrentYear(){
        Calendar fecha = Calendar.getInstance();
        return fecha.get(Calendar.YEAR);
    }

    public static String getStringHMS(Date fecha){
        String hms="";
        if(fecha!=null){
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            hms = hourFormat.format(fecha);
        }
        return hms;
    }

    public static String getStringYYMMDDHHMMSS(Date fecha){
        String yymmddhhmmss="";
        if(fecha!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            yymmddhhmmss = dateFormat.format(fecha);
        }
        return yymmddhhmmss;
    }

    public static String getStringFormatFromDate(Date fecha, String format){
        String yymmddhhmmss="";
        if(fecha!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            yymmddhhmmss = dateFormat.format(fecha);
        }
        return yymmddhhmmss;
    }

    public static String getStringDDMMYYYY(Date fecha){
        String ddmmyyyy="";
        if(fecha!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            ddmmyyyy = dateFormat.format(fecha);
        }
        return ddmmyyyy;
    }

    public static String getStringFechaActualYYMMDDHHMMSS(int sumaMilisegundos, int sumaSegundos, int sumaMinutos, int sumaHoras, int sumaDias, int sumaSemanas, int sumaMeses, int sumaAnios){
        Calendar fecha = Calendar.getInstance();

        if(sumaMilisegundos!=0){
            fecha.add(Calendar.MILLISECOND, sumaMilisegundos);
        }
        if(sumaSegundos!=0){
            fecha.add(Calendar.SECOND, sumaSegundos);
        }
        if(sumaMinutos!=0){
            fecha.add(Calendar.MINUTE, sumaMinutos);
        }
        if(sumaHoras!=0){
            fecha.add(Calendar.HOUR, sumaHoras);
        }
        if(sumaDias!=0){
            fecha.add(Calendar.DAY_OF_YEAR, sumaDias);
        }
        if(sumaSemanas!=0){
            fecha.add(Calendar.WEEK_OF_YEAR, sumaSemanas);
        }
        if(sumaMeses!=0){
            fecha.add(Calendar.MONTH, sumaMeses);
        }
        if(sumaAnios!=0){
            fecha.add(Calendar.YEAR, sumaAnios);
        }

        Date date = fecha.getTime();

        return getStringYYMMDDHHMMSS(date);
    }

    public static Date getDateFromStringEEEMMMDDHHMMSSZYYYY(String fecha){
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        try {
            Date date = formatter.parse(fecha);
            return date;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getDateFromStringYYYYMMMDD(String fecha){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Date date = formatter.parse(fecha);
            return date;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getDateFromStringDDMMYYYYHHMMSS(String fecha){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
        try {
            Date date = formatter.parse(fecha);
            return date;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getDateFromStringDDMMYYYY(String fecha){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        try {
            Date date = formatter.parse(fecha);
            return date;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String getStringFechaPrimerDiaVisibleCalendario(int mesVisible, int anioVisible){
        Calendar fecha = Calendar.getInstance();
        fecha.set(anioVisible, mesVisible, 1);
        while(fecha.get(Calendar.DAY_OF_WEEK)!=Calendar.MONDAY){
            fecha.add(Calendar.DAY_OF_YEAR,-1);
        }
        String fechaCriterio = FechaUtils.getStringDDMMYYYY(fecha.getTime());
        return fechaCriterio;
    }
    public static String getStringFechaUltimoDiaVisibleCalendario(int mesVisible, int anioVisible){
        Calendar fecha = Calendar.getInstance();
        fecha.set(anioVisible, mesVisible, 1);
        fecha.set(anioVisible, mesVisible, fecha.getActualMaximum(Calendar.DAY_OF_MONTH));
        while(fecha.get(Calendar.DAY_OF_WEEK)!=Calendar.SUNDAY){
            fecha.add(Calendar.DAY_OF_YEAR,1);
        }
        String fechaCriterio = FechaUtils.getStringDDMMYYYY(fecha.getTime());
        return fechaCriterio;
    }
    
    public static String getStringYYYYMMDDSeparatedLine(Date fecha){
        String ddmmyyyy="";
        if(fecha!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            ddmmyyyy = dateFormat.format(fecha);
        }
        return ddmmyyyy;
    }

    public static String getStringFullYYYYMMDDHHMMSS(Date fecha){
        String yymmddhhmmss="";
        if(fecha!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            yymmddhhmmss = dateFormat.format(fecha);
        }
        return yymmddhhmmss;
    }

    public static Date getDateFromFullYYYYMMDDHHMMSS(String fecha){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
        try {
            Date date = formatter.parse(fecha);
            return date;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static Long getYYYYMMDDtoNumber(Date fecha){
        Long numero = 0l;
        if(fecha != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String yymmddhhmmss = dateFormat.format(fecha);
            numero = Long.parseLong(yymmddhhmmss);
        }
        
        return numero;
    }
    
    public static Date getAdditionalMinute(Date startDate, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(Calendar.MINUTE, minute);

        return cal.getTime();
    }
    
    public static int diferenciaMinutosTwoStringDates(Date strDateUno, Date strDateDos){
        long elapsedms = strDateDos.getTime() - strDateUno.getTime();
        int diferencia = (int)TimeUnit.MINUTES.convert(elapsedms, TimeUnit.MILLISECONDS);
        
        return diferencia;
    }
}
