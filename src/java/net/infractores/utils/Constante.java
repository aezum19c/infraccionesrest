package net.infractores.utils;

public class Constante {
    public static final String URL_IMAGE_PUBlIC_SERVER = "http://34.235.20.167/infractores/assets/";
    public final static String VIEW_WEB_LOGIN = "WebLoginSanipes";
    public final static String VIEW_WEB_PRINCIPAL = "WebPrincipal";
    public final static String VIEW_WEB_SOLICITANTE = "WebSolicitante";
    public final static String VIEW_WEB_SUPERVISOR = "WebSupervisor";
    public final static String VIEW_WEB_INSPECTOR = "WebInspector";
    public final static String VIEW_REGISTRO_SOLICITUD = "Solicitante_RegistroSolicitud";
   
    public final static String ITEM_AREA_SOLICITANTE = "- Area Solicitante -";
    public final static String ITEM_ESTADO_SOLICITUD = "- Estado Solicitud -";
    public final static String LOOK_SUCCESS = "success";
    public final static String LOOK_DEFAULT = "default";
    public final static String LOOK_PRIMARY = "primary";
    public final static String LOOK_WARNING = "warning";
    public final static String LOOK_INFO = "info";
    public final static String LOOK_DANGER = "danger";
    public final static String LOOK_IMPORTANT = "important";
    
    public final static Integer NUMERO_GRANDE = 999999999;
    public final static Integer ACTIVO = 1;
    public final static Integer INACTIVO = 0;
    public final static Integer PERFIL_SOLICITANTE = 22;
    public final static Integer PERFIL_SUPERVISOR = 23;
    public final static Integer PERFIL_INSPECTOR = 24;
    
    public static final int SCOPE_SLEEP_ID = 1;
    public static final int SCOPE_HEART_ID = 2;
    public static final int SCOPE_STEPS_ID = 3;
    public static final int SCOPE_TEMPERATURE_ID = 4;
    public static final int SCOPE_SPO2_ID = 5;
    public static final int SCOPE_DISTANCE_ID = 6;
    
    public static final String SCOPE_SLEEP = "sleep";
    public static final String SCOPE_HEART = "heart";
    public static final String SCOPE_STEPS = "steps";
    public static final String SCOPE_TEMPERATURE = "temperature";
    public static final String SCOPE_DISTANCE = "distance";
    public static final String SCOPE_SPO2 = "spo2";
    
    public static final int DOMAIN_SERVCIO_REST_RUTA_LOG = 9;
    
    public static final String LOG_REST_SERVICE_GENERAL = "iSleep_RestService";
    
    public final static Integer INTEGER_PROCESO_OK = 1;
    public final static Integer INTEGER_ERROR = -1;
    public final static Integer DEVICE_DUPLICADO = -2;
    public final static Integer DEVICE_MACADDRESS_DUPLICADO = -3;
    
    //sw Para Gestionar Estandar de Errores
    public final static Integer PROCESO_EXITOSO = 200;
    public final static Integer ERROR_USUARIO_DATOS_INCORRECTOS = 401;
    public final static Integer ERROR_USUARIO_SIN_ROLES = 402;
    public final static Integer ERROR_SERVIDOR = 501;
    
    public static final String DESCRIPCION_PROCESO_EXITOSO = "";
    public static final String DESCRIPCION_ERROR_USUARIO_DATOS_INCORRECTOS = "Los datos ingresados son incorrectos";
    public static final String DESCRIPCION_ERROR_USUARIO_SIN_ROLES = "No tiene roles asignados";
    public static final String DESCRIPCION_ERROR_SERVIDOR = "Ocurrió un error";
}
